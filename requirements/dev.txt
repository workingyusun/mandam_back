-r ./common.txt

Werkzeug==0.14.1  # pyup: < 0.15 # https://github.com/pallets/werkzeug
ipdb==0.12.2  # https://github.com/gotcha/ipdb

# Testing
# ------------------------------------------------------------------------------
mypy==0.720  # https://github.com/python/mypy
pytest==5.1.1  # https://github.com/pytest-dev/pytest
pytest-sugar==0.9.2  # https://github.com/Frozenball/pytest-sugar
locustio==0.13.5  # https://docs.locust.io/en/latest/what-is-locust.html

# Code quality
# ------------------------------------------------------------------------------
flake8==3.7.8  # https://github.com/PyCQA/flake8
coverage==4.5.4  # https://github.com/nedbat/coveragepy
black==19.3b0  # https://github.com/ambv/black
pylint-django==2.0.11  # https://github.com/PyCQA/pylint-django

# Django
# ------------------------------------------------------------------------------
factory-boy==2.12.0  # https://github.com/FactoryBoy/factory_boy

django-extensions==2.2.1  # https://github.com/django-extensions/django-extensions
django-coverage-plugin==1.6.0  # https://github.com/nedbat/django_coverage_plugin
pytest-django==3.5.1  # https://github.com/pytest-dev/pytest-django
