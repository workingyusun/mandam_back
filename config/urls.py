"""redcap_be_boilerplate URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.contrib import admin
from django.contrib.sites.models import Site
from django.contrib.auth.views import PasswordResetConfirmView
from django.urls import path, re_path, include
from django.http import JsonResponse
from django.http import HttpResponseRedirect

from allauth.account.views import confirm_email
from rest_framework import routers
from rest_auth.registration.views import VerifyEmailView

from apps.accounts.check_plus import checkplus_main, checkplus_success
from apps.accounts.views import ProfileViewSet, MANADAMRegisterView, MANDAMLoginView, PhoneNumberViewSet, CheckPlusLogViewSet
from apps.accounts.views import HostApplicationViewSet, EmailAddressViewSet, InterestViewSet, KakaoLogin, FacebookLogin

from apps.core.utils import get_front_url
from apps.core.crons import alimtalk
from apps.core.views import BillKeyViewSet, PaymentLogViewSet

from apps.event.views import EventViewSet, ScheduleViewSet, RegistrationViewSet, TopSearchedViewSet, FeaturedTagViewSet, MainPageViewset
from apps.event.views import PromoCodeViewSet, LikedEventViewSet, MainBannerViewset, MagazineViewSet, MainMagazineViewSet
from apps.event.crons import make_schedules_disabled, payment_for_type_s_schedule, request_feedbacks, final_call_for_type_s

# TODO need to show something...
confirm_email_success = lambda request: JsonResponse({"status": "Done"})

router = routers.SimpleRouter()
router.register(r'accounts/profile', ProfileViewSet, basename="profile")
router.register(r'accounts/phone_number', PhoneNumberViewSet, basename="phone_number")
router.register(r'accounts/email_address', EmailAddressViewSet, basename="email_address")
router.register(r'accounts/interest', InterestViewSet, basename="interest")
router.register(r'accounts/host_application', HostApplicationViewSet, basename="host_application")
router.register(r'accounts/check_plus_log', CheckPlusLogViewSet, basename="check_plus_log")

router.register(r'card', BillKeyViewSet, basename="card")
router.register(r'payment_log', PaymentLogViewSet, basename="payment_log")

router.register(r'event', EventViewSet, basename="event")
router.register(r'schedule', ScheduleViewSet, basename="schedules")
router.register(r'registration', RegistrationViewSet, basename="registration")
router.register(r'top_searched', TopSearchedViewSet, basename="top_searched")
router.register(r'featured_tag', FeaturedTagViewSet, basename="featured_tag")
router.register(r'main_page', MainPageViewset, basename="main_page")
router.register(r'main_banner', MainBannerViewset, basename='main_banner')
router.register(r'promo_code', PromoCodeViewSet, basename="promo_code")
router.register(r'liked_event', LikedEventViewSet, basename='liked_event')
router.register(r'magazine', MagazineViewSet, basename='magazine')
router.register(r'main_magazine', MainMagazineViewSet, basename="main_magazine")

FRONT_SITE = Site.objects.filter(name="FRONT").first()
FRONT_DOMAIN = FRONT_SITE.domain if FRONT_SITE else "mandam.redcapx.com"
FRONT_URL = f"https://{FRONT_DOMAIN}/"


urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/', include(router.urls)),

    re_path(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
            PasswordResetConfirmView.as_view(template_name='account/password_reset.html'),
            name='password_reset_confirm'),
    # TODO need to redirect to front root.
    path('reset/done/', lambda request: HttpResponseRedirect(get_front_url()),
         name='password_reset_complete'),
    re_path(r'^api/v1/rest-auth/registration/account-confirm-email/(?P<key>[-:\w]+)/$',
            confirm_email, name='account_confirm_email'),
    path('accounts/registration/confirm-email-success/', confirm_email_success, name="confirm_email_success"),

    path('api/v1/rest-auth/login/', MANDAMLoginView.as_view(), name='rest_login'),
    path('api/v1/rest-auth/', include('rest_auth.urls')),
    path('api/v1/rest-auth/kakao/', KakaoLogin.as_view(), name='kakao_login'),
    path('api/v1/rest-auth/facebook/', FacebookLogin.as_view(), name="facebook_login"),

    path('api/v1/rest-auth/registration/', MANADAMRegisterView.as_view(), name="rest_register"),
    path('api/v1/rest-auth/registration/', include('rest_auth.registration.urls')),
    re_path(r'^account-confirm-email/', VerifyEmailView.as_view(),
            name='account_email_verification_sent'),
    # CRON
    path('make_schedules_disabled/', make_schedules_disabled, name="make_schedules_disabled"),
    path('payment_for_type_s_schedule/', payment_for_type_s_schedule, name="payment_for_type_s_schedule"),
    path('alimtalk/', alimtalk, name="alimtalk"),
    path('request_feedbacks/', request_feedbacks, name="request_feedbacks"),
    path('final_call_for_type_s/', final_call_for_type_s, name="final_call_for_type_s"),

    path('accounts/', include('allauth.urls')),
    
    # check_plus
    path('api/v1/accounts/checkplus_main/', checkplus_main, name="checkplus_main"),
    #path('api/v1/accounts/checkplus_test/', checkplus_test, name="checkplus_test"),
    path('api/v1/accounts/checkplus_success/', checkplus_success, name="checkplus_success"),

    re_path(r'^ckeditor/', include('ckeditor_uploader.urls')),
]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns += [path("__debug__/", include(debug_toolbar.urls))]
