from .base import *


print("prod.py loaded...")


INSTALLED_APPS = ["collectfast"] + INSTALLED_APPS  # noqa F405
# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ["14.52.29.153"]
# https://django-extensions.readthedocs.io/en/latest/installation_instructions.html#configuration
INSTALLED_APPS += ["django_extensions"]  # noqa F405
