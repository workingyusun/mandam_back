import json
import os
import environ

from django.core.exceptions import ImproperlyConfigured


# env에서 받아온 string을 boolean으로 casting 합니다. 
BOOLEAN_TRUE_STRINGS = ('true', 'on', 'ok', 'y', 'yes', '1')
_env_string_to_bool = lambda string: string in BOOLEAN_TRUE_STRINGS


class empty(object):
    pass

env = dict(os.environ.copy())

_local_env = environ.Env()
_local_env_path = os.path.join(os.path.dirname(__file__), '.env')
_local_env.read_env(_local_env_path)


def load_env(key, default=empty, cast=None):
    """
    환경 변수를 불러옵니다. (1순위: os.environ, 2순위: settings/.env)
    """
    value = None

    if key in env:
        print(f"{key} is loaded by os environ...")
        value = env[key]
    elif key in _local_env:
        print(f"{key} is loaded by settings/.env file...")
        value = _local_env.get_value(key)
    elif default == empty:
        error = f'Failed to load the value for the key : "{key}"; '
        error += 'Add the value for the key in settings/.env or export environment variable to ignore this error.'
        raise ImproperlyConfigured(error)
    else:
        print(f"{key} is loaded by default...")
        value = default
    
    if cast is bool and value is str:
        value = _env_string_to_bool(value.lower())
    if cast is int and value is str:
        value = int(value)

    return value
