from storages.backends.gcloud import GoogleCloudStorage  # noqa E402

GS_DEFAULT_ACL = "publicRead"


class StaticRootGoogleCloudStorage(GoogleCloudStorage):
    location = "static"
    default_acl = GS_DEFAULT_ACL


class MediaRootGoogleCloudStorage(GoogleCloudStorage):
    location = "media"
    file_overwrite = False
    default_acl = GS_DEFAULT_ACL
