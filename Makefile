PROJECT_ROOT := ${PWD}
PRIJECT_DB_NAME := rd
GCP_DEV_CLOUD_SQL_CONNECTION_NAME := mandamjp:asia-northeast1:mandam
DEFAULT_DATABASE_HOST := '127.0.0.1'
DEFAULT_DATABASE_PORT := 3307
DEFAULT_DATABASE_DATABASE := 'mandam_data'
DEFAULT_DATABASE_USER := 'root'
DEFAULT_DATABASE_PASSWORD := 'qwe123123'
DJANGO_SETTINGS_PROD := 'config.settings.prod'
DJANGO_GCP_STORAGE_BUCKET_NAME := 'mandamjp'
GOOGLE_APPLICATION_CREDENTIALS := '.gappcredentials.json'
M_ID := nicepay00m

e:
	echo $(PROJECT_ROOT)

################
# GCP commands #
################
gsqlproxy:
	./cloud_sql_proxy -instances="$(GCP_DEV_CLOUD_SQL_CONNECTION_NAME)"=tcp:3307


gapplogs:
	gcloud app logs tail

giamaccountslist:
	gcloud iam service-accounts list

giamsetpolicy:
	gcloud iam service-accounts set-iam-policy \
	redcapdjango@appspot.gserviceaccount.com \
	.giampolicy.json


################
################
migratelocal:
	python manage.py migrate

migrateprod:
	DJANGO_SETTINGS_MODULE=$(DJANGO_SETTINGS_PROD) \
	DEFAULT_DATABASE_HOST=$(DEFAULT_DATABASE_HOST) \
	DEFAULT_DATABASE_PORT=$(DEFAULT_DATABASE_PORT) \
	DEFAULT_DATABASE_DATABASE=$(DEFAULT_DATABASE_DATABASE) \
	DEFAULT_DATABASE_USER=$(DEFAULT_DATABASE_USER) \
	DEFAULT_DATABASE_PASSWORD=$(DEFAULT_DATABASE_PASSWORD) \
	DJANGO_GCP_STORAGE_BUCKET_NAME=$(DJANGO_GCP_STORAGE_BUCKET_NAME) \
	M_ID=$(M_ID) \
	python manage.py migrate

collectprod:
	DJANGO_SETTINGS_MODULE=$(DJANGO_SETTINGS_PROD) \
	DEFAULT_DATABASE_HOST=$(DEFAULT_DATABASE_HOST) \
	DEFAULT_DATABASE_PORT=$(DEFAULT_DATABASE_PORT) \
	DEFAULT_DATABASE_DATABASE=$(DEFAULT_DATABASE_DATABASE) \
	DEFAULT_DATABASE_USER=$(DEFAULT_DATABASE_USER) \
	DEFAULT_DATABASE_PASSWORD=$(DEFAULT_DATABASE_PASSWORD) \
	DJANGO_GCP_STORAGE_BUCKET_NAME=$(DJANGO_GCP_STORAGE_BUCKET_NAME) \
	GOOGLE_APPLICATION_CREDENTIALS=$(GOOGLE_APPLICATION_CREDENTIALS) \
	python manage.py collectstatic

deploy: migrateprod
	./deploy-gae.sh


#baseline:
#	mysql -h ${TYRANNO_DB_HOST} \
#	-P ${TYRANNO_DB_PORT} \
#	-u${TYRANNO_DB_USER} \
#	-p$(TYRANNO_DB_PASSWORD) \
#	${TYRANNO_DB_NAME} < $(FLYWAY_BASELINE_FILE_PATH)


#migrate:
#	flyway \
#	-url=$(FLYWAY_URL) \
#	-user=$(TYRANNO_DB_USER) \
#	-password=$(TYRANNO_DB_PASSWORD) \
#	-locations=$(FLYWAY_MIGRATIONS_LOCATIONS) \
#	-schemas=$(PRIJECT_DB_NAME) \
#	migrate


#exsqlfile:
#	mysql -h 127.0.0.1 \
#	-P 3306 \
#	-uroot \
#	-p \
#	${TYRANNO_DB_NAME} < $(FLYWAY_BASELINE_FILE_PATH)
#
#
#migrate_local:
#	flyway \
#	-url=jdbc:mysql://localhost:3306?serverTimezone=UTC \
#	-user=root \
#	-password= \
#	-locations=$(FLYWAY_MIGRATIONS_LOCATIONS) \
#	-schemas=$(PRIJECT_DB_NAME) \
#	migrate
