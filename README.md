# A django boilerplate for REDCAP

## Before you get started

1. Clone this repo.

2. Change a remote's URL.

    ```bash
    git remote set-url origin https://github.com/USERNAME/REPOSITORY.git
    ```

3. Push this to your repository.
4. Change values in settings/.env and app.yaml

## features

1. .env
    * To get environ values in this project, please call **_load_env_** function in settings/loader.py.

        ```python
        from .loader import load_env

        DEBUG = load_env("DJANGO_DEBUG", False, cast=bool)
        ```

    * The value will be evaluated by searching in **os.envion** first, and if it is missing, the loader will try to read the value in **setting/.env** file second.
    * the loader will raise an **ImproperlyConfigured** Error if both of them are missing and the param for default is not passed.

2. Custom error handler

    ```json
    # a validation error
    {
        "detail": {
            "user": [
                "Profile with this user already exists."
            ]
        },
        "code": 999999
    }

    # an error in view logics
    # you can make this error message with make_error_message function in $ROOT_DIR/apps/core/utils.py
    {
        "detail": "This user's phone number is not registered.",
        "code": 100000
    }
    ```

3. Auth
  
  ```bash
    http://localhost:8000/api/v1/rest-auth/login/ -> login
    http://localhost:8000/api/v1/rest-auth/registration/ -> signup
    http://localhost:8000/api/v1/rest-auth/password/reset/ -> password reset
  ```

* if you want to change rest-auth settings, check out account settings in settings/base.py
  
    ```python
    # django-allauth
    # ------------------------------------------------------------------------------
    ACCOUNT_ALLOW_REGISTRATION = load_env("DJANGO_ACCOUNT_ALLOW_REGISTRATION", True)
    # https://django-allauth.readthedocs.io/en/latest/configuration.html
    # ACCOUNT_AUTHENTICATION_METHOD = "username"
    ACCOUNT_AUTHENTICATION_METHOD = "email"
    # https://django-allauth.readthedocs.io/en/latest/configuration.html
    ACCOUNT_EMAIL_REQUIRED = True
    ACCOUNT_USERNAME_REQUIRED = False
    ACCOUNT_UNIQUE_EMAIL = True
    ACCOUNT_SIGNUP_PASSWORD_ENTER_TWICE = True
    ACCOUNT_SESSION_REMEMBER = True
    ACCOUNT_EMAIL_MAX_LENGTH = 190
    ACCOUNT_CONFIRM_EMAIL_ON_GET = True
    ACCOUNT_LOGOUT_REDIRECT_URL = '/'
    # https://django-allauth.readthedocs.io/en/latest/configuration.html
    ACCOUNT_EMAIL_VERIFICATION = "mandatory"
    ACCOUNT_EMAIL_CONFIRMATION_COOLDOWN = 3
    # https://django-allauth.readthedocs.io/en/latest/configuration.html
    ACCOUNT_ADAPTER = "apps.accounts.adapters.AccountAdapter"
    # https://django-allauth.readthedocs.io/en/latest/configuration.html
    ACCOUNT_LOGOUT_ON_GET = True
    EMAIL_CONFIRMATION_REDIRECT_URL = "/accounts/registration/confirm-email-success/"
    ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = EMAIL_CONFIRMATION_REDIRECT_URL
    ACCOUNT_EMAIL_CONFIRMATION_AUTHENTICATED_REDIRECT_URL = EMAIL_CONFIRMATION_REDIRECT_URL
    ```

4. REMOTE BUCKET & DB & deploy

