import calendar
import datetime

from django.db.models import OuterRef, Subquery

from pytz import timezone, utc

from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from apps.event.models import Schedule, Event, Registration

from .alimtalk import make_alimtalk, bulk_make_alimtalk_with_same_message
from .serializers import BillKeyRegisterSerializer, BillKeyRetrieveSerializer, PaymentLogSerializer
from .models import BillKey, PaymentLog
from .payment_utils import get_bill_key, delete_bill_key, cancel
from .utils import make_error_message, get_kt, get_now

UTC = timezone("UTC")
KST = timezone('Asia/Seoul')

class BillKeyViewSet(
    viewsets.GenericViewSet,
    mixins.CreateModelMixin,
    mixins.ListModelMixin,
    mixins.RetrieveModelMixin,
    mixins.DestroyModelMixin):
    queryset = BillKey.objects.all()
    serializer_class = BillKeyRetrieveSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user).order_by('-created_at')

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        delete_bill_key(instance)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def create(self, request, *args, **kwargs):
        serializer = BillKeyRegisterSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)

        data = serializer.validated_data
        bk_log = get_bill_key(
            user=data.get('user'),
            card_no=data.get('card_no'),
            card_pw=data.get('card_pw'),
            exp_year=data.get('exp_year'),
            exp_month=data.get('exp_month'),
            id_no=data.get('id_no')
        )

        if bk_log.result_code == "F100":
            if not BillKey.objects.filter(user=request.user, card_no=bk_log.card_no).exists():
                bk = BillKey(
                    user=bk_log.user,
                    bid=bk_log.bid,
                    card_name=bk_log.card_name,
                    card_no=bk_log.card_no,
                    exp_year=bk_log.exp_year,
                    exp_month=bk_log.exp_month,
                )
                bk.save()
                serializer = BillKeyRetrieveSerializer(bk)
                headers = self.get_success_headers(serializer.data)
                return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
            else:
                return Response(make_error_message(400001), status=status.HTTP_400_BAD_REQUEST)
        return Response(make_error_message(400000), status=status.HTTP_400_BAD_REQUEST)


class PaymentLogViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    queryset = PaymentLog.objects.all()
    serializer_class = PaymentLogSerializer
    permission_classes = [IsAuthenticated, ]

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user).order_by('-created_at')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        self.pagination_class.page_size = 6

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=["get"], detail=True)
    def cancel(self, request, *args, **kwargs):
        instance = self.get_object()
        regi = instance.registration

        # check this is already canceled.
        if regi.status != "payment_success":
            return Response(make_error_message(100016), status=status.HTTP_400_BAD_REQUEST)

        schedule = regi.schedule

        cancel_amt = regi.price
        partial_cancel_code = 0

        now = get_now()
        start_at = schedule.start_at
        if start_at <= now + datetime.timedelta(days=1):
            return Response(make_error_message(100016), status=status.HTTP_400_BAD_REQUEST)
        elif now + datetime.timedelta(days=1) < start_at <= now + datetime.timedelta(days=5):
            cancel_amt = int(cancel_amt/100*50)
            partial_cancel_code = 1
        elif now + datetime.timedelta(days=5) < start_at <= now + datetime.timedelta(days=7):
            cancel_amt = int(cancel_amt/100*80)
            partial_cancel_code = 1

        is_succeed = cancel(instance, cancel_amt, partial_cancel_code)

        if is_succeed:
            event = schedule.event
            regi.status = "payment_canceled"
            regi.is_confirmed = False
            regi.save()
            instance.canceled_amt = cancel_amt
            instance.save()
            alimtalk_kwargs = {
                "event_name": event.title,
                "schedule_name": schedule.title,
                "schedule_datetime": get_kt(schedule.start_at).strftime("%Y/%m/%d %H:%M:%S"),
                "purchase_date": get_kt(instance.updated_at).strftime("%Y/%m/%d %H:%M:%S"),
                "price": cancel_amt,
                "payment_method": f"{instance.card_name} {instance.card_no}"
            }
            template_code = "MANDAM0006"
            make_alimtalk(
                to=instance.user.phonenumber.phone_number,
                template_code=template_code,
                **alimtalk_kwargs
            )

        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    @action(methods=["get"], detail=False)
    def adjustment(self, request, *args, **kwargs):
        user = request.user

        if not user.profile.is_host:
            return Response(status=status.HTTP_403_FORBIDDEN)

        year = request.query_params.get('year')
        month = request.query_params.get('month')

        if not year or not month:
            return Response(make_error_message(100017), status=status.HTTP_400_BAD_REQUEST)

        year = int(year)
        month = int(month)
        last_day = calendar.monthrange(year, month)[1]

        start = datetime.datetime(int(year), int(month), 1).astimezone(KST)
        end = datetime.datetime(int(year), int(month), last_day, 23, 59, 59).astimezone(KST)

        status_dict = {
            "payment_success": "결제 완료",
            "payment_canceled": "취소 완료"
        }

        schedules = Subquery(
            Schedule.objects.filter(
                start_at__gte=start, start_at__lte=end, event__user=user).values('id'))
        regis = Subquery(
            Registration.objects.filter(
                schedule__in=schedules, status__in=status_dict.keys()).values('id'))
        pls = PaymentLog.objects.filter(registration__in=regis).select_related('registration__schedule__event')

        data = []

        for pl in pls:
            regi = pl.registration
            schedule = regi.schedule
            event = schedule.event
            data.append({
                "tid": pl.tid,
                "nickname": regi.user.profile.nickname,
                "purchase_date": pl.created_at,
                "schedule_date": schedule.start_at,
                "schedule_title": schedule.title,
                "event_title": event.title,
                "schedule_price": schedule.price,
                "discount": schedule.price - regi.price,
                "price": regi.price,
                "status": status_dict[regi.status],
                "adjustment_amount": regi.price - pl.canceled_amt
            })
        return Response(data)
