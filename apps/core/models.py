from django.conf import settings

from django.db import models


class AlimtalkLog(models.Model):
    to = models.CharField("수신자", max_length=20)
    template_code = models.CharField("메시지 코드", max_length=50)
    content = models.TextField("내용")
    has_button = models.BooleanField(default=False)
    button_name = models.CharField(max_length=20, blank=True, default="")
    button_link = models.CharField(max_length=200, blank=True, default="")

    is_sent = models.BooleanField("전송완료", default=False)
    status = models.CharField(max_length=20, default="", blank=True)
    error_message = models.TextField(default="", blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.to} - {self.template_code}"
    
    class Meta:
        verbose_name = '결제 알림톡 전송내역'
        verbose_name_plural = '결제 알림톡 전송내역'


class BillKeyLog(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="bill_key_logs",
        on_delete=models.CASCADE,
    )
    result_code = models.TextField(default="", blank=True)
    result_msg = models.TextField("등록 요청 결과", default="", blank=True)
    bid = models.TextField(default="", blank=True)
    auth_date = models.TextField(default="", blank=True)
    card_no = models.TextField(default="", blank=True)
    card_code = models.TextField(default="", blank=True)
    card_name = models.TextField(default="", blank=True)
    tid = models.TextField(default="", blank=True)
    exp_year = models.TextField(default="", blank=True)
    exp_month = models.TextField(default="", blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user.username}: {self.card_name}"

    @property
    def email(self):
        return self.user.email

    class Meta:
        verbose_name = '카드 등록 요청 내역'
        verbose_name_plural = '카드 등록 요청 내역'


class PaymentLog(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="payment_logs",
        on_delete=models.CASCADE,
    )
    result_code = models.TextField(default="", blank=True)
    result_msg = models.TextField(default="", blank=True)
    auth_code = models.TextField(default="", blank=True)
    auth_date = models.TextField(default="", blank=True)

    acqu_card_code = models.TextField(default="", blank=True)
    acqu_card_name = models.TextField(default="", blank=True)
    card_no = models.TextField(default="", blank=True)
    card_code = models.TextField(default="", blank=True)
    card_name = models.TextField(default="", blank=True)
    card_quota = models.TextField(default="", blank=True)
    card_interest = models.TextField(default="", blank=True)
    card_cl = models.TextField(default="", blank=True)

    amt = models.TextField("결제금액", default="", blank=True)
    goods_name = models.TextField(default="", blank=True)
    mid = models.TextField(default="", blank=True)
    moid = models.TextField(default="", blank=True)
    buyer_name = models.TextField(default="", blank=True)
    tid = models.TextField(default="", blank=True)

    canceled_amt = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user.username}: {self.goods_name}-{self.card_name}"

    @property
    def email(self):
        return self.user.email

    @property
    def payment(self):
        payment = self.amt.lstrip("0")
        return payment

    class Meta:
        verbose_name = '결제 내역'
        verbose_name_plural = '결제 내역'


class PaymentCancelLog(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="payment_cancel_logs",
        on_delete=models.CASCADE,
    )
    payment_log = models.ForeignKey(
        PaymentLog,
        related_name="payment_cancel_logs",
        on_delete=models.CASCADE,
    )
    result_code = models.TextField(default="", blank=True)
    result_msg = models.TextField(default="", blank=True)
    canceled_amt = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user.username} - {self.created_at}"

    @property
    def email(self):
        return self.user.email

    class Meta:
        verbose_name = '결제 취소 내역'
        verbose_name_plural = '결제 취소 내역'


class BillKey(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="bill_keys",
        on_delete=models.CASCADE,
    )
    card_no = models.TextField("카드번호", default="", blank=True)
    card_name = models.TextField("카드명", default="", blank=True)
    bid = models.TextField(default="", blank=True)
    exp_year = models.TextField("카드 만료년", default="", blank=True)
    exp_month = models.TextField("카드 만료일", default="", blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user.username}: {self.card_name}"
