import datetime
from pytz import timezone, utc

from django.conf import settings
from django.contrib.sites.models import Site

from rest_framework.views import exception_handler

KST = timezone('Asia/Seoul')
UTC = timezone("UTC")

ERROR_CODES = {
    100000: "이 모임은 관리자의 승인이 필요합니다.",
    100001: "이 일정에 지원자가 있습니다,",
    100002: "이 일정은 마감되었습니다.",
    100003: "모임 타입이 잘못 되었습니다.",
    100004: "이 일정은 매진 되었습니다.",
    100005: "프로모코드를 잘못 입력하셨습니다.",
    100006: "이 프로모코드는 이미 사용하셨습니다.",
    100007: "이 일정의 가격은 프로모 코드의 최저 금액보다 낮습니다.",
    100008: "직접 선택 일정만 참가자를 선택 할 수 있습니다.",
    100009: "인원이 초과되었습니다.",
    100010: "게스트 선정은 한번만 가능합니다",
    100011: "게스트 선정 기한이 지났습니다.",
    100012: "게스트는 한명 이상 선정해야합니다",
    100013: "이 프로모 코드는 이미 사용되었습니다.",
    100014: "이 모임은 이미 찜하셨습니다",
    100015: "유효한 카드가 아닙니다.",
    100016: "정상적인 환불요청이 아닙니다.",
    100017: "년, 월 정보가 없습니다.",
    200000: "이 모임은 거절되었습니다.",
    200001: "이 모임은 검토중입니다.",
    200002: "임시저장 모드로 돌아갈 수 없습니다.",
    200003: "모임의 타입은 변경할 수 없습니다.",
    200004: "이 일정에 신청한 게스트가 존재합니다.",
    200005: "이 모임은 참가신청 취소가 불가합니다.",
    200006: "이 모임은 참가신청 취소가 불가합니다.",

    300001: "회원가입이 필요합니다.",
    300002: "이 번호는 이미 존재합니다.",
    300003: "만 14세 이상만 가입 가능합니다",

    400000: "이 카드는 유효하지 않습니다.",
    400001: "이 카드는 이미 등록하셨습니다.",
}


make_error_message = lambda error_code: {"detail": ERROR_CODES.get(error_code), "code": error_code}


def exception_handler_with_error_code(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    response = exception_handler(exc, context)

    if response is not None:
        data = {}
        data['detail'] = response.data.copy()
        # 999999 is for unexpected errors.
        data['code'] = getattr(exc, 'code', 999999)
        response.data = data

    return response


def settings_context(_request):
    return {"settings": settings}


def get_now():
    return datetime.datetime.utcnow().astimezone(UTC)


def get_utc(dt):
    return dt.astimezone(UTC)


def get_kt(dt):
    return dt.astimezone(KST)

def get_front_url():
    front_site = Site.objects.filter(name="FRONT").first()
    front_domain = front_site.domain if front_site else "mandam.redcapx.com"
    front_url = f"https://{front_domain}"
    return front_url

def get_back_url():
    site = Site.objects.filter(pk=1).first()
    domain = site.domain if site else "api.mandam.redcapx.com"
    back_url = f"https://{domain}"
    return back_url
