import hashlib
import hmac
import base64
import time
import json
import requests

from .alimtalk_templates import TEMPLATES
from .models import AlimtalkLog

ALIMTALK_SERVICE_ID = "ncp:kkobizmsg:kr:2586126:redcapsule"
ALIMTALK_HOST = "https://sens.apigw.ntruss.com"
ALIMTALK_URI = f"/alimtalk/v2/services/{ALIMTALK_SERVICE_ID}/messages"
ALIMTALK_CHANNEL = "@레드캡엑스"
ALIMTALK_ACCESS_KEY = "h9yJ7AultBbAExIMnolX"
ALIMTALK_SECERT_KEY = "p0ZvObIurS1kfznJ63m0PsyJY4gcmWPKMOau5ZIP"

_get_timestamp = lambda: str(int(time.time() * 1000))


def _make_signature(method="POST"):
    timestamp = _get_timestamp()
    secret_key = bytes(ALIMTALK_SECERT_KEY, 'UTF-8')
    uri = ALIMTALK_URI
    message = method + " " + uri + "\n" + timestamp + "\n" + ALIMTALK_ACCESS_KEY
    message = bytes(message, 'UTF-8')
    signing_key = base64.b64encode(hmac.new(secret_key, message, digestmod=hashlib.sha256).digest())
    return signing_key


def send_alimtalk():
    messages = AlimtalkLog.objects.filter(is_sent=False)[:100]
    message_dict = {}

    for m in messages:
        m_list = message_dict.get(m.template_code, [])
        m_list.append(m)
        message_dict[m.template_code] = m_list

    for template_code, m_list in message_dict.items():
        message_list = []

        for m in m_list:
            message = {
                "to":m.to,
                "content": m.content,
                "buttons":[]
            }
            if m.has_button:
                button = {
                    "type":"WL",
                    "name": m.button_name,
                    "linkMobile": m.button_link,
                    "linkPc": m.button_link,
                }
                message['buttons'] = [button, ]

            message_list.append(message)

        body = {
            "plusFriendId": ALIMTALK_CHANNEL,
            "templateCode": template_code,
            "messages": message_list
        }
        headers = {
            "Content-Type": "application/json; charset=utf-8",
            "x-ncp-apigw-timestamp": _get_timestamp(),
            "x-ncp-iam-access-key": ALIMTALK_ACCESS_KEY,
            "x-ncp-apigw-signature-v2": _make_signature()
        }
        res = requests.post(ALIMTALK_HOST+ALIMTALK_URI, data=json.dumps(body), headers=headers)

        for m in m_list:
            m.status = res.status_code
            if not res.ok:
                m.error_message = res.json().get('errorMessage', '')
            m.is_sent = True

        AlimtalkLog.objects.bulk_update(m_list, ["is_sent", "status", "error_message"])


def make_alimtalk(to, template_code, button_link_kwargs={}, **kwargs):
    template = TEMPLATES.get(template_code)

    if not template:
        return None

    message = template.get('message').format(**kwargs)

    alim_kwargs = {
        "to": to,
        "template_code": template_code,
        "content": message
    }

    if template.get('has_button', False):
        alim_kwargs.update({
            "has_button": True,
            "button_name": template.get("button_name"),
            "button_link": template.get('button_link').format(**button_link_kwargs)
        })

    al = AlimtalkLog(**alim_kwargs)
    al.save()


def bulk_make_alimtalk_with_same_message(to_list, template_code, button_link_kwargs={}, **kwargs):
    template = TEMPLATES.get(template_code)

    if not template:
        return None

    message = template.get('message').format(**kwargs)
    al_list = []
    for to in to_list:
        alim_kwargs = {
            "to": to,
            "template_code": template_code,
            "content": message
        }

        if template.get('has_button', False):
            alim_kwargs.update({
                "has_button": True,
                "button_name": template.get("button_name"),
                "button_link": template.get('button_link').format(**button_link_kwargs)
            })
        al = AlimtalkLog(**alim_kwargs)
        al_list.append(al)

    AlimtalkLog.objects.bulk_create(al_list, 100)


def bulk_make_alimtalk(message_list):
    al_list = []

    for message in message_list:
        template_code = message.get('template_code', None)
        template = TEMPLATES.get(template_code)

        if not template:
            continue

        message_kwargs = message.get("kwargs", {})
        alim_kwargs = {
            "to": message.get("to"),
            "template_code": template_code,
            "content": template.get('message').format(**message_kwargs)
        }

        if template.get('has_button', False):
            button_link_kwargs = message.get('button_link_kwargs', {})
            alim_kwargs.update({
                "has_button": True,
                "button_name": template.get("button_name"),
                "button_link": template.get('button_link').format(**button_link_kwargs)
            })

        al = AlimtalkLog(**alim_kwargs)
        al_list.append(al)

    AlimtalkLog.objects.bulk_create(al_list, 100)
