import datetime
import hashlib
import logging

from random import randint

import requests

from Crypto.Cipher import AES
from .models import BillKeyLog, PaymentLog, PaymentCancelLog



logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)


BS = AES.block_size
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
#unpad = lambda s: s[0:-ord(s[-1])]

HOST = "https://webapi.nicepay.co.kr/webapi"
MID = "nictest04m"
MOID = "1004"
MERCHANT_KEY = "b+zhZ4yOZ7FsH8pm5lhDfHZEb79tIwnjsdA0FBXh86yLc6BJeFVrZFXhAoJ3gEWgrWwN+lJMV0W4hvDdbe4Sjw=="


class AESCipher:
    def __init__(self, key):
        self.key = key

    def encrypt(self, raw):
        """
        Returns hex encoded encrypted value!
        """
        raw = pad(raw)
        cipher = AES.new(self.key, AES.MODE_ECB)
        return cipher.encrypt(raw).hex()

    # def decrypt(self, enc):
    #     """
    #     Requires hex encoded param to decrypt
    #     """
    #     enc = enc.decode("hex")
    #     cipher = AES.new(self.key, AES.MODE_ECB)
    #     return unpad(cipher.decrypt(enc))


def get_edi_date():
    return datetime.datetime.today().strftime("%Y%m%d%H%M%S")


def get_sign_data(string):

    encoded_str = string.encode()
    encrypted_data = hashlib.sha256(encoded_str).hexdigest()
    return encrypted_data


def nice_request(url, data={}, params={}, raise_error=False):
    headers = {
        'Content-type': 'application/x-www-form-urlencoded',
        'charset': 'euc-kr'
    }
    response = requests.post(
        url=url,
        data=data,
        params=params,
        headers=headers
    )
    if raise_error:
        response.raise_for_status()
    if response.ok:
        return response.json()
    return


def get_bill_key(user, card_no, card_pw, exp_year, exp_month, id_no):
    edi_date = get_edi_date()
    string = MID + edi_date + MOID + MERCHANT_KEY

    # Get sign data
    sign_data = get_sign_data(string)

    #Get Enc Data
    plain = f"CardNo={card_no}&ExpYear={exp_year}&ExpMonth={exp_month}&IDNo={id_no}&CardPw={card_pw}"
    aes = AESCipher(MERCHANT_KEY[:16])
    enc_text = aes.encrypt(plain)
    data = {"MID": MID, "Moid": MOID, "SignData": sign_data, "EncData": enc_text, "EdiDate": edi_date}

    url = f"{HOST}/billing/billing_regist.jsp"

    res = nice_request(url, params=data, raise_error=True)
    bk_log = BillKeyLog(
        user=user,
        result_code=res.get("ResultCode", ""),
        result_msg=res.get("ResultMsg", ""),
        bid=res.get("BID", ""),
        auth_date=res.get("AuthDate", ""),
        card_no=card_no,
        card_code=res.get("CardCode", ""),
        card_name=res.get("CardName", ""),
        tid=res.get("TID", ""),
        exp_year=exp_year,
        exp_month=exp_month,
    )
    bk_log.save()
    return bk_log

def get_tid():
    # 01: 지불수단 (신용카드) -> 체크카드도 가능 함
    # 16: 빌링
    return MID + "01" + "16" + datetime.datetime.today().strftime("%y%m%d%H%M%S") + str(randint(1000, 9999))

def payment(user, bill_key, goods_name, amt):
    edi_date = get_edi_date()
    string = MID + edi_date + MOID + str(amt) + bill_key + MERCHANT_KEY
    sign_data = get_sign_data(string)
    tid = get_tid()

    params = {
        "BID": bill_key,
        "MID": MID,
        "TID": tid,
        "EdiDate": edi_date,
        "Moid": MOID,
        "Amt": amt,
        "GoodsName": goods_name,
        "SignData": sign_data,
        "CardInterest": "0",
        "CardQuota": "00",
    }
    url = f"{HOST}/billing/billing_approve.jsp"

    is_succeed = True
    try:
        res = nice_request(url=url, params=params, raise_error=True)
    except:
        is_succeed = False

    if res.get('ResultCode', "") != "3001":
        is_succeed = False

    pl = PaymentLog(
        user=user,
        result_code=res.get("ResultCode", ""),
        result_msg=res.get("ResultMsg", ""),
        auth_code=res.get('AuthCode', ""),
        auth_date=res.get('AuthDate', ""),
        acqu_card_code=res.get('AcquCardCode', ""),
        acqu_card_name=res.get("AcquCardName", ""),
        card_no=res.get("CardNo", ""),
        card_code=res.get("CardCode", ""),
        card_name=res.get("CardName", ""),
        card_quota=res.get("CardQuota", ""),
        card_interest=res.get("CardInterest", ""),
        card_cl=res.get("CardCl", ""),
        amt=res.get("Amt", ""),
        tid=res.get("TID", ""),
        goods_name=goods_name,
        mid=res.get("MID", ""),
        moid=res.get("Moid", ""),
        buyer_name=res.get("BuyerName", ""),
    )
    pl.save()
    return pl, is_succeed


def cancel(payment_log, cancel_amt, partial_cancel_code=0):
    edi_date = get_edi_date()
    string = MID + str(cancel_amt) + edi_date + MERCHANT_KEY
    sign_data = get_sign_data(string)
    params = {
        "TID": payment_log.tid,
        "MID": MID,
        "Moid": MOID,
        "CancelAmt": cancel_amt,
        "CancelMsg": "고객 요청",
        "PartialCancelCode": partial_cancel_code,
        "EdiDate": edi_date,
        "SignData": sign_data
    }
    url = f"{HOST}/cancel_process.jsp"
    is_succeed = True

    try:
        res = nice_request(url=url, params=params, raise_error=True)
    except:
        is_succeed = False

    pcl = PaymentCancelLog(
        user=payment_log.user,
        payment_log=payment_log,
        canceled_amt=cancel_amt,
        result_code=res.get("ResultCode", ""),
        result_msg=res.get("ResultMsg", ""),
    )
    pcl.save()
    return is_succeed

def delete_bill_key(bk):
    edi_date = get_edi_date()
    string = MID + edi_date + MOID + bk.bid + MERCHANT_KEY
    sign_data = get_sign_data(string)
    data = {"MID": MID, "Moid": MOID, "SignData": sign_data, "BID": bk.bid, "EdiDate": edi_date}
    url = f"{HOST}/billing/billkey_remove.jsp"
    res = nice_request(url, params=data)
