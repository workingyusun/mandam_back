from django.http import JsonResponse
from .alimtalk import send_alimtalk


def alimtalk(request):
    is_cron = request.headers.get("X-Appengine-Cron", False)

    if not is_cron:
        return JsonResponse({"status": "fail"})

    send_alimtalk()

    return JsonResponse({"status": "done"})
