from rest_framework import serializers

from apps.event.serializers import RegistrationListSerializer, PromoCodeReadOnlySerializer
from apps.event.models import PromoCodeLog
from .models import BillKey

class BillKeyRegisterSerializer(serializers.Serializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    card_no = serializers.CharField()
    card_pw = serializers.CharField()
    exp_year = serializers.CharField()
    exp_month = serializers.CharField()
    id_no = serializers.CharField()


class BillKeyRetrieveSerializer(serializers.ModelSerializer):
    class Meta:
        model = BillKey
        fields = ["id", "card_no", "card_name", "exp_year", "exp_month"]


class RegistrationWithPromocodeSerializer(RegistrationListSerializer):
    promocode = serializers.SerializerMethodField()

    def get_promocode(self, instance):
        user = instance.user
        schedule = instance.schedule
        promocode_log = PromoCodeLog.objects.filter(user=user, schedule=schedule).first()
        if promocode_log:
            return PromoCodeReadOnlySerializer(promocode_log.promo_code, read_only=True).data
        else:
            return None


class PaymentLogSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    registration = RegistrationWithPromocodeSerializer(read_only=True)
    card_name = serializers.CharField(read_only=True)
    card_no = serializers.CharField(read_only=True)
    amt = serializers.CharField(read_only=True)
    canceled_amt = serializers.IntegerField(read_only=True)
    created_at = serializers.CharField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

