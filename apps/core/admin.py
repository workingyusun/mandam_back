from django.contrib import admin

from .models import AlimtalkLog, BillKeyLog, BillKey, PaymentLog, PaymentCancelLog

admin.site.register(AlimtalkLog)


@admin.register(PaymentLog)
class PaymentLogAdmin(admin.ModelAdmin):
    list_display = ['user', 'email', 'goods_name', 'card_name', 'payment', 'result_msg', 'created_at']
    raw_id_fields = ('user', )
    search_fields = ['user__email']


@admin.register(PaymentCancelLog)
class PaymentLogCancelLogAdmin(admin.ModelAdmin):
    list_display = ['user', 'email', 'payment_log', 'canceled_amt', 'created_at',]
    raw_id_fields = ('user',)
    search_fields = ['user__email']


@admin.register(BillKeyLog)
class BillKeyLogAdmin(admin.ModelAdmin):
    list_display = ['user', 'email', 'card_name', 'result_msg']
    raw_id_fields = ('user',)
    search_fields = ['user__email']