TEMPLATES = {
    "MANDAM0001": {
        "message":"""
[ 취존만담 신청 완료 ]

취존만담 '{event_name}'에 신청이 완료되었습니다.모임과 관련된 정보는 최소 3일 이전에 호스트께서 별도로 안내드릴 예정입니다!

기타 문의사항은 '취존만담' 카카오톡채널 채팅방에 메시지 남겨주세요 :)

모임 결제 내역
▶ 모임명 : {event_name}
▶ 일정명 : {schedule_name}
▶ 일정 시작 : {schedule_datetime}

▶ 결제일 : {purchase_date}
▶ 결제금액 : {price}
▶ 결제수단 : {payment_method}

취존만담 드림
""",
        "has_button": True,
        "button_link": "https://mandam.redcapx.com/reservations",
        "button_name": "확인하기",
    },
    "MANDAM0002": {
        "message": """
[ 취존만담 결제 실패 ] 

신청하신 '{event_name}'의 결제가 실패했습니다. 다른 결제수단을 등록해주세요.

모임 결제 내역
▶ 모임명 : {event_name}
▶ 일정명 : {schedule_name}
▶ 일정 시작 : {schedule_datetime}

▶ 결제일 : {purchase_date}
▶ 결제금액 : {price}
▶ 결제수단 : {payment_method}

취존만담 드림
""",
        "has_button": False,
    },
    "MANDAM0003": {
        "message": """
[ 취존만담 신청 완료 ]

취존만담 '{event_name}'에 신청이 완료되셨습니다. 초대 여부를 결정해서 모임 최소 7일 전까지 안내드리도록 할께요.

모임 신청 내용
▶ 모임명 : {event_name}
▶ 일정명 : {schedule_name}
▶ 일정 시작 : {schedule_datetime}

* 호스트가 모임 초대를 결정한 후에는 등록된 카드로 참가비가 자동 결제되며, 확정되기 전까지는 결제가 이뤄지지 않습니다 :)

취존만담 드림
    """,
        "has_button": True,
        "button_link": "https://mandam.redcapx.com/reservations",
        "button_name": "확인하기",
    },
    # 11번과 바뀌어야함.
    "MANDAM0004": {
        "message": """
[ 취존만담 참석 확정 ]

신청하신 '{event_name}'에 참가가 확정되었습니다! 참가비는 등록되신 카드로 자동 결제 되었어요 :)

모임과 관련된 정보는 최소 3일 이전에 호스트를 통해 별도로 안내드릴 예정이며,
기타 문의사항은 '취존만담' 카카오톡채널 채팅방에 메시지 남겨주세요 :)

모임 결제 내역
▶ 일정명 : {schedule_name}
▶ 일정 시작 : {schedule_datetime}

▶ 결제일 : {purchase_date}
▶ 결제금액 : {price}
▶ 결제수단 : {payment_method}

취존만담 드림
""",
        "has_button": False,
    },
    "MANDAM0005": {
        "message": """
[ 취존만담 취소 안내 ] 

취존만담 '{event_name}'에 관심을 가지고 신청해주셨는데 아쉽게도 모시지 못해 너무 아쉬워요 :( 
너그러운 양해를 부탁드리며, 다음 기회에 취존만담에서 다시 만나요 :)

취존만담 드림
""",
        "has_button": True,
        "button_link": "https://mandam.redcapx.com/reservations",
        "button_name": "확인하기",
    },
    "MANDAM0006": {
        "message": """
[ 취존만담 환불 안내 ] 

신청하신 '{event_name}'이 환불 처리 되었습니다. 다음 기회에 취존만담에서 다시 만나요 :)

모임 환불 내역
▶ 일정명 : {schedule_name}
▶ 일정 시작 : {schedule_datetime}

▶ 결제일 : {purchase_date}
▶ 결제금액 : {price}
▶ 결제수단 : {payment_method}
* 결제하신 내역은 환불 정책에 따라 영업일 기준 5일 내에 처리 됩니다.

취존만담 드림
""",
        "has_button": False,
    },
    "MANDAM0007": {
        "message": """
[ 취존만담 후기 ]

어제 참여하신 {host_name}님의 '{event_name}'은 즐거우셨나요?

잠시 시간을 내어 모임 경험에 대해 간단한 후기를 작성해주세요 :)  {guest_name}님이 남겨주시는 소중한 후기 하나하나가 나에게는 추억으로, 호스트에게는 행복함과 더 나은 다음을 준비하는데 도움이 됩니다.

{host_name}님도 {guest_name}님에 대한 만족도를 별점으로 남길 수도 있고, {guest_name}님만 볼 수 있는 비공개 후기를 작성할 수도 있습니다. 

앞으로도 다채롭고 즐거운 만남을 만들어가는 취존만담이 되겠습니다.

취존만담 드림
""",
        "has_button": True,
        "button_link": "https://mandam.redcapx.com/reservations/{registration_id}/review",
        "button_name": "후기 작성하기",
    },
    "MANDAM0008": {
        "message": """
[ 취존만담 모임 안내 ]

신청하신 '{event_name}'이 승인 되었습니다! 이제 주최한 모임의 일정관리에 들어가서 모임 일정 추가해주세요.

일정 추가와 더불어, '모임 주최 가이드'에 있는 모임 운영과 관련된 사항들도 꼼꼼히 체크해주시길 바랄께요 :)

취존만담 드림
""",
        "has_button": True,
        "button_link": "https://mandam.redcapx.com/events/my",
        "button_name": "확인하기",
    },
    "MANDAM0009": {
        "message": """
[ 취존만담 모임 안내 ]

호스트님께서 개설한 모임 '{event_name}'의 참가자들을 오늘까지 꼭 선정해 주세요! 미 선정 시에는 모임이 취소되오니, 금일 자정까지 반드시 참가자 선정을 완료해주시기 바랍니다 :)

* 호스트님의 과실로 인해 모임이 취소될 시 이후 진행하는 모임에 대해서는 제재가 있을 수 있으니, 이 점 유념해 주시기를 바랍니다.

취존만담 드림
""",
        "has_button": True,
        "button_link": "https://mandam.redcapx.com/schedules/{schedule_id}/manage",
        "button_name": "확인하기",
    },
    "MANDAM0010": {
        "message": """
[ 취존만담 후기 ]

어제 진행하신 '{event_name}'은 어떠셨나요? 잠시 시간을 내어 같이 한 분들에 대한 간단한 후기를 남겨주세요. 작성해주시는 소중한 후기 하나하나가 함께하신 분들에게는 좋은 추억을 남겨 준답니다 :)

취존만담 드림
""",
        "has_button": True,
        "button_link": "https://mandam.redcapx.com/schedules/{schedule_id}/manage",
        "button_name": "후기작성하기",
    },    
    "MANDAM0011": {
        "message": """
[ 취존만담 참석 확정 ]

신청하신 '{event_name}'에 참가가 확정되었습니다! 참가비는 등록되신 카드로 자동 결제 되었어요 :)

모임과 관련된 정보는 최소 3일 이전에 호스트를 통해 별도로 안내드릴 예정이며,
기타 문의사항은 '취존만담' 카카오톡채널 채팅방에 메시지 남겨주세요 :)

모임 결제 내역
▶ 일정명 : {schedule_name}
▶ 일정 시작 : {schedule_datetime}

▶ 결제일 : {purchase_date}
▶ 결제금액 : {price}
▶ 결제수단 : {payment_method}

취존만담 드림
""",
        "has_button": True,
        "button_link": "https://mandam.redcapx.com/reservations",
        "button_name": "확인하기",
    },
}
