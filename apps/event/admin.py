from django.contrib import admin
from rangefilter.filter import DateRangeFilter

from .models import Event, EventImage, EventQuestion, Schedule
from .models import Registration, EventReply, TopSearched, FeaturedTag
from .models import MainPage, PromoCode, PromoCodeLog
from .models import HostFeedback, GuestFeedback, LikedEvent
from .models import MainBanner, Magazine, MainMagazine


admin.site.register(TopSearched)
admin.site.register(FeaturedTag)


class ScheduleInlineAdmin(admin.TabularInline):
    model = Schedule


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ['id', 'user', 'email', 'title', 'event_type', 'tags', 'is_draft', 'is_verified', ]
    search_fields = ["title", 'user__email', ]
    raw_id_fields = ('user',)
    inlines = [ScheduleInlineAdmin]

    list_filter = (
        ('event_type', admin.ChoicesFieldListFilter),
        ('is_draft', admin.BooleanFieldListFilter),
        ('is_verified', admin.BooleanFieldListFilter),
        ('is_declined', admin.BooleanFieldListFilter),
        ('created_at', DateRangeFilter),
        ('updated_at', DateRangeFilter),
    )


@admin.register(EventImage)
class EventImageAdmin(admin.ModelAdmin):
    list_display = ['event_title' ,'is_main']
    search_fields = ["event__title", ]
    raw_id_fields = ('event',)


@admin.register(EventQuestion)
class EventQuestionAdmin(admin.ModelAdmin):
    list_display = ['event_title' ,'shorcut_content']
    search_fields = ["event__title", ]
    raw_id_fields = ('event',)


@admin.register(Schedule)
class ScheduleAdmin(admin.ModelAdmin):
    list_display = ['event_title' ,'title', 'event_type', 'quota', 'price', 'is_closed', 'start_at', ]
    search_fields = ["event__title", ]
    raw_id_fields = ('event',)
    list_filter = (
        ('start_at', DateRangeFilter),
        ('end_at', DateRangeFilter),
        ('is_closed', admin.BooleanFieldListFilter),
    )


@admin.register(Registration)
class RegistrationAdmin(admin.ModelAdmin):
    list_display = ['id', 'schedule_title', 'user_id', 'user_email', 'status', 'is_confirmed', 'schedule_start_at', ]
    search_fields = ["schedule_title", 'user__email',]
    raw_id_fields = ('schedule', 'user', 'payment_log')

    list_filter = (
        ('is_confirmed', admin.BooleanFieldListFilter),
        ('status', admin.ChoicesFieldListFilter),
        ('schedule_start_at', DateRangeFilter),
        ('schedule_end_at', DateRangeFilter),
    )


@admin.register(PromoCode)
class PromoCodeAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'code', 'min_price', 'discount', 'start_at', 'end_at', ]
    search_fields = ["title", 'code', "discount", ]
    list_filter = (
        ('start_at', DateRangeFilter),
        ('end_at', DateRangeFilter),
    )


@admin.register(MainPage)
class MainPageAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'tag', ]


@admin.register(MainBanner)
class MainBannerAdmin(admin.ModelAdmin):
    list_display = ['id', 'image', 'link', ]


@admin.register(PromoCodeLog)
class PromoCodeLogAdmin(admin.ModelAdmin):
    list_display = ['promo_code', 'email', 'schedule_id', 'schedule_title', ]
    raw_id_fields = ('schedule', 'user', 'promo_code')
    search_fields = ['promo_code__title', 'user__email', ]


@admin.register(HostFeedback)
class HostFeedbackAdmin(admin.ModelAdmin):
    list_display = ['user_email', 'registration_title', 'host_email', 'rate', 'rate_of_satisfaction', 'rate_of_program', 'rate_of_space', ]
    raw_id_fields = ('user', 'host', 'registration', )
    search_fields = ["host__email", ]


@admin.register(GuestFeedback)
class GuestFeedbackAdmin(admin.ModelAdmin):
    list_display = ['user_email', 'guest_email', 'rate', 'feedback', ]
    raw_id_fields = ('user', 'guest', )
    search_fields = ['guest__email',]


@admin.register(LikedEvent)
class LikedEventAdmin(admin.ModelAdmin):
    list_display = ['user_id', 'event_title', ]
    raw_id_fields = ('user', 'event', )
    # search_fields = ["event__title", 'user__email', ]


@admin.register(Magazine)
class MagazineAdmin(admin.ModelAdmin):
    list_display = ['id', 'title', 'subtitle', 'keyword', 'is_main', ]


@admin.register(MainMagazine)
class MainMagazineAdmin(admin.ModelAdmin):
    raw_id_fields = ('magazine', )
