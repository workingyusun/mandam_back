import datetime
import logging


from django.http import JsonResponse

from apps.core.models import BillKey
from apps.core.utils import get_now, get_kt
from apps.core.payment_utils import payment
from apps.core.alimtalk import make_alimtalk, bulk_make_alimtalk

from .models import Schedule, Registration


logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)


def make_schedules_disabled(request):
    is_cron = request.headers.get("X-Appengine-Cron", False)

    if not is_cron:
        return JsonResponse({"status": "fail"})

    now = get_now().replace(minute=0, second=0)

    queryset =  Schedule.objects.filter(is_closed=False)
    fsfc_schedule_queryset = queryset.filter(event__event_type="F", start_at__lte=now+datetime.timedelta(days=1))
    select_schedule_queryset = queryset.filter(event__event_type="S", start_at__lte=now+datetime.timedelta(days=8))

    queryset = fsfc_schedule_queryset | select_schedule_queryset

    schedule_list = []

    for schedule in queryset:
        schedule.is_closed = True
        schedule_list.append(schedule)

    Schedule.objects.bulk_update(schedule_list, ["is_closed"])
    return JsonResponse({"status": "done"})


def payment_for_type_s_schedule(request):
    is_cron = request.headers.get("X-Appengine-Cron", False)

    if not is_cron:
        return JsonResponse({"status": "fail"})

    regis = Registration.objects.filter(
        schedule__event_type="S", status="payment_ready"
        ).select_related('schedule__event').order_by('id')[:50]
    message_list = []

    for regi in regis:
        regi.status = "payment_started"
        regi.save()
        is_succeed = False
        card = None
        try:
            # 카드를 지웠을 경우
            card = BillKey.objects.filter(id=regi.card_id).first()
            if card:
                payment_log, is_succeed = payment(
                    regi.user,
                    card.bid,
                    regi.schedule.title,
                    regi.price
                )
            else:
                payment_log = None

            if is_succeed:
                regi.status = "payment_success"
                regi.is_confirmed = True
            else:
                regi.status = "payment_failed"
            regi.payment_log = payment_log
            regi.save()
        except Exception as e:
            regi.status = "payment_failed"
            regi.is_confirmed = False
            regi.save()
            logger.critical(e)
            is_succeed = False

        phone_number = regi.user.phonenumber.phone_number

        if is_succeed:
            template_code = "MANDAM0004"
        else:
            template_code = "MANDAM0002"
        
        if card:
            payment_method = f"{card.card_name} {card.card_no}"
        else:
            payment_method = "카드가 존재하지 않습니다."

        message_list.append({
            "to": regi.user.phonenumber.phone_number,
            "template_code": template_code,
            "kwargs": {
                "event_name": regi.schedule.event.title,
                "schedule_name": regi.schedule.title,
                "schedule_datetime": get_kt(regi.schedule.start_at).strftime("%Y/%m/%d %H:%M:%S"),
                "purchase_date": get_kt(regi.updated_at).strftime("%Y/%m/%d %H:%M:%S"),
                "price": regi.price,
                "payment_method": payment_method
            }
        })

    bulk_make_alimtalk(message_list)

    # for message in message_list:
    #     alimtalk_kwargs = message.get('kwargs')
    #     make_alimtalk(
    #         to=message.get('to'),
    #         template_code=message.get('template_code'),
    #         **alimtalk_kwargs
    #     )

    return JsonResponse({"status": "done"})


def request_feedbacks(request):
    is_cron = request.headers.get("X-Appengine-Cron", False)

    if not is_cron:
        return JsonResponse({"status": "fail"})

    now = get_now().replace(minute=0, second=0)

    schedules = Schedule.objects.filter(
        start_at__gte=now-datetime.timedelta(days=1), end_at__lte=now
    ).prefetch_related('registrations').select_related('event')

    for schedule in schedules:
        host = schedule.event.user
        alim_kwargs = {
            "event_name": schedule.event.title
        }
        make_alimtalk(
            to=host.phonenumber.phone_number,
            template_code="MANDAM0010",
            button_link_kwargs={"schedule_id": schedule.id},
            **alim_kwargs
        )
        regis = schedule.registrations.filter(is_confirmed=True)
        message_list = []
        for regi in regis:
            guest = regi.user
            alim_kwargs = {
                "host_name": host.profile.nickname,
                "event_name": schedule.event.title,
                "guest_name":guest.profile.nickname
            }
            message_list.append({
                "to": regi.user.phonenumber.phone_number,
                "template_code": "MANDAM0007",
                "kwargs": alim_kwargs,
                "button_link_kwargs": {"registration_id": regi.id}
            })
        bulk_make_alimtalk(message_list)

    return JsonResponse({"status": "done"})


def final_call_for_type_s(request):
    is_cron = request.headers.get("X-Appengine-Cron", False)

    if not is_cron:
        return JsonResponse({"status": "fail"})

    now = get_now().replace(minute=0, second=0)

    queryset = Schedule.objects.filter(is_closed=False)
    select_schedule_queryset = queryset.filter(
        event__event_type="S", start_at__gte=now+datetime.timedelta(days=10), 
        start_at__lte=now+datetime.timedelta(days=9)
    )

    message_list = []

    for schedule in select_schedule_queryset:
        alim_kwargs = {
            "event_name": schedule.event.title
        }
        message_list.append({
            "to": schedule.event.user.phonenumber.phone_number,
            "template_code": "MANDAM0009",
            "kwargs": alim_kwargs,
            "button_link_kwargs": {"schedule_id": schedule.id}
        })

    bulk_make_alimtalk(message_list)

    return JsonResponse({"status": "done"})
