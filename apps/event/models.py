from django.conf import settings
from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models
from imagekit.models import ProcessedImageField
from imagekit.processors import Thumbnail, ResizeToFill


from apps.core.alimtalk import make_alimtalk
from apps.core.models import PaymentLog

from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

EVENT_TYPE_FCFS = "F"
EVENT_TYPE_SELECT = "S"
EVENT_TYPE_CHOICES = (
    (EVENT_TYPE_FCFS, '선착순'),
    (EVENT_TYPE_SELECT, '선택'),
)
RATE_TYPE_EXCELENT = "E"
RATE_TYPE_GOOD = "G"
RATE_TYPE_BAD = "B"
RATE_TYPE_CHOICES = (
    (RATE_TYPE_EXCELENT, 'EXCELENT'),
    (RATE_TYPE_GOOD, "GOOD"),
    (RATE_TYPE_BAD, "BAD")
)


class MainBanner(models.Model):
    image = models.ImageField("배너 이미지", upload_to='uploads/main_banner/%Y/%m/%d/')
    link = models.URLField("링크", default="", blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.pk}"

    class Meta:
        verbose_name = '메인화면_배너'
        verbose_name_plural = '메인화면_배너'


class MainPage(models.Model):
    title = models.CharField(max_length=100)
    tag = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '메인화면'
        verbose_name_plural = '메인화면'


class TopSearched(models.Model):
    content = models.CharField(max_length=50)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.content

    class Meta:
        verbose_name = '인기 검색어'
        verbose_name_plural = '인기 검색어'


class FeaturedTag(models.Model):
    tag = models.CharField( max_length=50)
    # image = models.ImageField(upload_to='uploads/featured_tag/%Y/%m/%d/')
    # ProcessedImageField(upload_to='uploads/event/main_image/%Y/%m/%d/', null=True, format='JPEG', options={'quality': 60})
    image = ProcessedImageField( upload_to='uploads/featured_tag/%Y/%m/%d/', format='JPEG', processors=[Thumbnail(128, 128)], options={'quality': 60})
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.tag

    class Meta:
        verbose_name = '인기 카테고리'
        verbose_name_plural = '인기 카테고리'


class EventManager(models.Manager):
    def verified(self):
        return self.get_queryset().filter(is_verified=True)

    def verified_fcfs(self):
        return self.verified().filter(event_type=EVENT_TYPE_FCFS)

    def verified_select(self):
        return self.verified().filter(event_type=EVENT_TYPE_SELECT)


class Event(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="events",
        on_delete=models.CASCADE,
    )
    title = models.CharField("모임 이름", max_length=50)
    event_type = models.CharField("참가자 모집 유형", max_length=1, choices=EVENT_TYPE_CHOICES)
    address = models.CharField("모임 주최장소", max_length=100)
    address_detail = models.CharField("모임 주최 장소 상세", max_length=100)
    tel = models.CharField("주최자 연락처", max_length=30)
    desc = models.TextField("모임 상세 설명")
    tags = models.TextField("태그", blank=True)
    is_draft = models.BooleanField(default=True)
    is_verified = models.BooleanField("모임 승인", default=False)
    is_declined = models.BooleanField("모임 반려", default=False)
    # main_image = models.ImageField(upload_to='uploads/event/main_image/%Y/%m/%d/', null=True)
    main_image = ProcessedImageField(upload_to='uploads/event/main_image/%Y/%m/%d/', null=True, format='JPEG', processors=[Thumbnail(572, 416)], options={'quality': 60})
    last_schedule_price = models.PositiveIntegerField("최근 업데이트된 참가비용", default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    objects = EventManager()

    def __str__(self):
        return self.title

    __original_is_verified = None

    def __init__(self, *args, **kwargs):
        super(Event, self).__init__(*args, **kwargs)
        self.__original_is_verified = self.is_verified


    def save(self, *args, **kwargs):
        if not self.__original_is_verified:
            if self.__original_is_verified != self.is_verified:
                alimtalk_kwargs = {
                    "event_name": self.title
                }
                make_alimtalk(
                    to=self.user.phonenumber.phone_number,
                    template_code="MANDAM0008",
                    **alimtalk_kwargs
                )
        # enforce is_verfied False when is_draft is True
        if self.is_draft:
            self.is_verified = False
        super(Event, self).save(*args, **kwargs)

    @property
    def email(self):
        return self.user.email

    class Meta:
        verbose_name = '모임'
        verbose_name_plural = '모임'


class EventImage(models.Model):
    event = models.ForeignKey(
        Event,
        related_name='images',
        on_delete=models.CASCADE
    )
    # image = models.ImageField(upload_to='uploads/event/%Y/%m/%d/')
    image = ProcessedImageField(upload_to='uploads/event/%Y/%m/%d/', processors=[Thumbnail(572, 416)], format='JPEG', options={'quality': 60})
    is_main = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.event.title

    def save(self, *args, **kwargs):
        if self.is_main:
            event = self.event
            event.main_image = self.image
            event.save()
        super(EventImage, self).save(*args, **kwargs)
    
    @property
    def event_title(self):
        return self.event.title

    class Meta:
        verbose_name = '모임 이미지'
        verbose_name_plural = '모임 이미지'
        

class EventQuestion(models.Model):
    event = models.ForeignKey(
        Event,
        related_name='questions',
        on_delete=models.CASCADE
    )
    content = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.content

    @property
    def event_title(self):
        return self.event.title

    @property
    def shorcut_content(self):
        return self.content[:20]

    class Meta:
        verbose_name = '모임 참여 신청 질문'
        verbose_name_plural = '모임 참여 신청 질문'


class PromoCode(models.Model):
    title = models.CharField("코드 이름", max_length=30)
    code = models.CharField("코드", max_length=30, unique=True)
    min_price = models.PositiveIntegerField("최소사용금액")
    discount = models.PositiveIntegerField("할인금액")
    start_at = models.DateTimeField("코드 사용 시작일")
    end_at = models.DateTimeField("코드 사용 마감일")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '프로모션 코드'
        verbose_name_plural = '프로모션 코드'


class Schedule(models.Model):
    event = models.ForeignKey(
        Event,
        related_name="schedules",
        on_delete=models.CASCADE
    )
    title = models.CharField("일정 이름", max_length=100)
    event_type = models.CharField("참가자 모집 유형", max_length=1, choices=EVENT_TYPE_CHOICES, default="F")
    start_at = models.DateTimeField("일정 시작일")
    end_at = models.DateTimeField("일정 종료일")
    quota = models.PositiveSmallIntegerField("참가 인원수") # 0 to 32767
    price = models.PositiveIntegerField("참가비용")
    is_closed = models.BooleanField("일정 종료", default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def participant_count(self):
        return self.registrations.filter(status="payment_success").count()
    
    @property
    def event_title(self):
        return self.event.title

    @property
    def registration_count(self):
        return self.registrations.count()

    def __str__(self):
        return f"{self.event.title}: {self.title}"

    def save(self, *args, **kwargs):
        event = self.event
        event.last_schedule_price = self.price
        event.save()
        self.event_type = event.event_type
        super(Schedule, self).save(*args, **kwargs)

    class Meta:
        verbose_name = '모임 일정'
        verbose_name_plural = '모임 일정'
        

class PromoCodeLog(models.Model):
    promo_code = models.ForeignKey(PromoCode, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    schedule = models.ForeignKey(Schedule, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.promo_code.code}, {self.user.username}"

    @property
    def schedule_title(self):
        return self.schedule.title

    @property
    def email(self):
        return self.user.email

    class Meta:
        verbose_name = '프로모션 코드 사용 내역'
        verbose_name_plural = '프로모션 코드 사용 내역'


class Registration(models.Model):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="registrations",
        on_delete=models.CASCADE,
    )
    schedule = models.ForeignKey(
        Schedule,
        related_name="registrations",
        on_delete=models.DO_NOTHING
    )
    schedule_title = models.CharField("일정 제목", max_length=100, default="", blank=True)
    schedule_start_at = models.DateTimeField("모임 시작일", null=True, blank=True)
    schedule_end_at = models.DateTimeField("모임 종료일", null=True, blank=True)
    event_tags = models.TextField("태그", blank=True, default="")
    is_confirmed = models.BooleanField("이벤트 승인여부", default=False)
    status = models.CharField(
        "사용자 결제상태",
        max_length=30,
        default='ready',
        choices=(
            ('ready', '승인대기'),
            ('payment_ready', '결제대기'),
            ('payment_started', "결재진행중"), # 직접 선정 일정을 위한 choice
            ('payment_success', '결제성공'),
            ('payment_failed', '결제실패'),
            ("payment_canceled", "결제취소"),
            ('dropped', "탈락"),
        )
    )
    price = models.PositiveIntegerField("참가비용", default=0)

    card_id = models.PositiveIntegerField("사용자 결제정보", null=True)
    payment_log = models.OneToOneField(
        PaymentLog,
        on_delete=models.DO_NOTHING,
        null=True
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.schedule_title}, {self.user.profile.name}"
    
    @property
    def user_email(self):
        return self.user.email

    def save(self, update_fields=None, *args, **kwargs):
        if not self.schedule_title:
            self.schedule_title = self.schedule.title
        if not self.schedule_start_at:
            self.schedule_start_at = self.schedule.start_at
        if not self.schedule_end_at:
            self.schedule_end_at = self.schedule.end_at
        if not self.event_tags:
            self.event_tags = self.schedule.event.tags
        if self.schedule.event.event_type == "F" and self.status == "payment_success":
            self.is_confirmed = True
        if self.price == 0:
            self.price = self.schedule.price
        super(Registration, self).save(*args, **kwargs)

    class Meta:
        verbose_name = '모임 참여 내역'
        verbose_name_plural = '모임 참여 내역'


class EventReply(models.Model):
    registration = models.ForeignKey(
        Registration,
        related_name="event_replies",
        on_delete=models.CASCADE
    )
    event_question = models.ForeignKey(
        EventQuestion,
        related_name="event_replies",
        on_delete=models.CASCADE
    )
    question = models.TextField(blank=True, default="")
    content = models.TextField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.question}, {self.content}"

    def save(self, *args, **kwargs):
        self.question = self.event_question.content
        super(EventReply, self).save(*args, **kwargs)


class HostFeedback(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="my_feedbacks_to_hosts")
    host = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
        related_name="feedbacks_from_guests", null=True, blank=True
    )
    registration = models.OneToOneField(Registration, on_delete=models.CASCADE, related_name="host_feedback")
    rate = models.PositiveIntegerField("별점", validators=[MaxValueValidator(5), MinValueValidator(1)])
    rate_of_satisfaction = models.CharField("만족도", choices=RATE_TYPE_CHOICES, default="E", max_length=1)
    rate_of_program = models.CharField("프로그램 만족도", choices=RATE_TYPE_CHOICES, default="E", max_length=1)
    rate_of_space = models.CharField("장소 만족도", choices=RATE_TYPE_CHOICES, default="E", max_length=1)
    feedback = models.TextField("후기", blank=True, default="")
    # image = models.ImageField(upload_to='uploads/feedback/%Y/%m/%d/', null=True)
    image = ProcessedImageField("사진", upload_to='uploads/feedback/%Y/%m/%d/', null=True, format='JPEG', options={'quality': 60})
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user.username}, {self.host.username}"

    @property
    def host_email(self):
        return self.host.email

    @property
    def user_email(self):
        return self.user.email

    @property
    def registration_title(self):
        return self.registration.schedule_title

    def save(self, *args, **kwargs):
        if not self.host or self.host != self.registration.schedule.event.user:
            self.host = self.registration.schedule.event.user
        super(HostFeedback, self).save(*args, **kwargs)

    class Meta:
        verbose_name = '호스트 평가'
        verbose_name_plural = '호스트 평가'


class GuestFeedback(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="my_feedbacks_to_guests")
    guest = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
        related_name="feedbacks_from_hosts", null=True, blank=True
    )
    registration = models.OneToOneField(Registration, on_delete=models.CASCADE, related_name="guest_feedback")
    rate = models.PositiveIntegerField("만족도", validators=[MaxValueValidator(5), MinValueValidator(1)])
    feedback = models.TextField("후기", blank=True, default="")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.user.username}, {self.guest.username}"

    def save(self, *args, **kwargs):
        if not self.guest or self.guest != self.registration.user:
            self.guest = self.registration.user
        super(GuestFeedback, self).save(*args, **kwargs)

    @property
    def user_email(self):
        return self.user.email

    @property
    def guest_email(self):
        return self.guest.email

    @property
    def registration_title(self):
        return self.registration.schedule_title

    class Meta:
        verbose_name = '게스트 평가'
        verbose_name_plural = '게스트 평가'


class LikedEvent(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="liked_events")
    event = models.ForeignKey(Event, on_delete=models.CASCADE, related_name="liked_events")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    @property
    def event_title(self):
        return self.event.title

    class Meta:
        verbose_name = '이벤트 - 사용자 목록'
        verbose_name_plural = '이벤트 - 사용자 목록'


class Magazine(models.Model):
    title = models.CharField("제목", max_length=100)
    subtitle = models.CharField("부제목", max_length=100)
    content = RichTextUploadingField("컨텐츠")
    keyword = models.CharField("키워드", max_length=100)
    image = models.ImageField("이미지", upload_to='uploads/magazine/%Y/%m/%d/')
    is_main = models.BooleanField("메인에 게시", default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title
    
    class Meta:
        verbose_name = '매거진'
        verbose_name_plural = '매거진'


class MainMagazine(models.Model):
    magazine = models.ForeignKey(Magazine, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.magazine.title
    
    class Meta:
        verbose_name = '메인화면_매거진'
        verbose_name_plural = '메인화면_매거진'
    