import datetime

from django.db.models import Avg
from drf_writable_nested.serializers import WritableNestedModelSerializer
from rest_framework import serializers

from apps.accounts.serializers import ProfileSerializer, ProfileReadOnlySerializer, ProfileHostReadOnlySerializer
from apps.core.models import BillKey
from apps.core.utils import get_now, get_kt, get_utc

from .models import Event, EventImage, EventQuestion, Schedule, MainPage
from .models import Registration, EventReply, TopSearched, FeaturedTag
from .models import GuestFeedback, HostFeedback, PromoCode, LikedEvent
from .models import Magazine, MainMagazine


class MainBannerSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.ImageField(read_only=True)
    link = serializers.URLField(read_only=True)


class MainPageSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(read_only=True)
    tag = serializers.CharField(read_only=True)


class TopSearchedSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    content = serializers.CharField(read_only=True)


class FeaturedTagSerialzier(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    tag = serializers.CharField(read_only=True)
    image = serializers.ImageField(read_only=True)


class PromoCodeReadOnlySerializer(serializers.Serializer):
    title = serializers.CharField(read_only=True)
    code = serializers.CharField(read_only=True)
    min_price = serializers.IntegerField(read_only=True)
    discount = serializers.IntegerField(read_only=True)
    start_at = serializers.DateTimeField(read_only=True)
    end_at = serializers.DateTimeField(read_only=True)


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = "__all__"

    def validate_event(self, value):
        if not value.is_verified:
            raise serializers.ValidationError("모임이 승인되지 않았습니다.")
        return value

    def validate_start_at(self, value):
        now = get_now()
        now_kt = get_kt(now)
        now_kt_date = now_kt.date()

        value_utc = get_utc(value)
        value_kt = get_kt(value_utc)
        value_kt_date = value_kt.date()

        event = Event.objects.get(pk=self.initial_data.get("event"))

        if value_kt_date <= now_kt_date:
            raise serializers.ValidationError("모임에 일정은 오늘 이후로만 만들 수 있습니다.")

        if event.event_type == "S":
            now_kt_date += datetime.timedelta(days=7)

            if value_kt_date <= now_kt_date:
                raise serializers.ValidationError("모임에 8일 이후 일정만 만들 수 있습니다.")

        return value

    def validate_end_at(self, value):
        start_at = datetime.datetime.strptime(self.initial_data.get('start_at'), '%Y-%m-%d %H:%M:%S')
        start_at_utc = get_utc(start_at)

        if value < start_at_utc:
            raise serializers.ValidationError("종료 날짜는 시작 날짜 이후여야 합니다.")

        return value

    def validate_price(self, value):
        if value < 1000:
            raise serializers.ValidationError("가격은 1000원 이상부터 가능합니다.")
        return value


class ScheduleReadOnlySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(read_only=True)
    start_at = serializers.DateTimeField(read_only=True)
    end_at = serializers.DateTimeField(read_only=True)
    quota = serializers.IntegerField(read_only=True)
    price = serializers.IntegerField(read_only=True)
    is_closed = serializers.BooleanField(read_only=True)
    event_type = serializers.CharField(read_only=True)
    participant_count = serializers.IntegerField(read_only=True)
    registration_count = serializers.IntegerField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)


class EventImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventImage
        fields = ["image", "is_main"]


class EventImageReadOnlySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.ImageField(read_only=True)
    is_main = serializers.BooleanField(read_only=True)


class EventQuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = EventQuestion
        fields = ["id", "content"]


class EventQuestionReadOnlySerialzier(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    content = serializers.CharField(read_only=True)


class EventBaseSerialzier(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(read_only=True)
    event_type = serializers.CharField(read_only=True)
    tags = serializers.CharField(read_only=True)
    main_image = serializers.ImageField(read_only=True)


class EventListSerializer(EventBaseSerialzier):
    last_schedule_price = serializers.IntegerField(read_only=True)
    liked_event = serializers.IntegerField(read_only=True)


class EventMeSerialzier(EventBaseSerialzier):
    is_draft = serializers.BooleanField(read_only=True)
    is_verified = serializers.BooleanField(read_only=True)
    is_declined = serializers.BooleanField(read_only=True)


class EventRetrieveSerializer(EventBaseSerialzier):
    address = serializers.CharField(read_only=True)
    address_detail = serializers.CharField(read_only=True)
    tel = serializers.CharField(read_only=True)
    desc = serializers.CharField(read_only=True)
    is_draft = serializers.BooleanField(read_only=True)
    is_verified = serializers.BooleanField(read_only=True)
    is_declined = serializers.BooleanField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)

    questions = EventQuestionReadOnlySerialzier(many=True, read_only=True)
    #schedules = ScheduleReadOnlySerializer(many=True, read_only=True)
    profile = ProfileReadOnlySerializer(read_only=True, source="user.profile")
    images = EventImageReadOnlySerializer(read_only=True, many=True)


class ScheduleRetrieveSerializer(ScheduleReadOnlySerializer):
    event = EventRetrieveSerializer(read_only=True)


class EventUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Event
        fields = [
            'id', 'title', 'event_type', 'address', "address_detail",
            "tel", "desc", "tags", "is_draft"
        ]


class EventSerializer(WritableNestedModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    profile = ProfileSerializer(read_only=True, source="user.profile")
    images = EventImageSerializer(many=True)
    last_schedule_price = serializers.SerializerMethodField()
    questions = EventQuestionSerializer(many=True, required=False)
    # schedules = ScheduleSerializer(many=True, read_only=True)

    class Meta:
        model = Event
        fields = "__all__"
        read_only_field = ["is_verified", ]

    def validate_user(self, value):
        if not value.profile.is_host:
            raise serializers.ValidationError("먼저 호스트 신청을 해주세요.")
        return value

    def validate_images(self, value):
        if not value:
            raise serializers.ValidationError("이미지를 등록해주세요.")

        flag = False

        for v in value:
            if v['is_main']:
                flag = True
                break

        if not flag:
            raise serializers.ValidationError("메인 이미지가 없습니다.")

        return value

    def validate_questions(self, value):
        event_type = self.initial_data.get("event_type")
        if not value and event_type == "S":
            raise serializers.ValidationError("질문을 입력해주세요.")

        if value and event_type == "F":
            raise serializers.ValidationError("질문 입력은 직접 선택 이벤트에서만 가능합니다.")

        return value

    def get_last_schedule_price(self, instance):
        last_schedule = instance.schedules.last()
        return last_schedule.price if last_schedule else 0


class EventReplySerializer(serializers.ModelSerializer):
    class Meta:
        model = EventReply
        fields = ["event_question", "content"]


class EventReplyReadSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    question = serializers.CharField(read_only=True)
    content = serializers.CharField(read_only=True)


class HostFeedbackReadOnlySerializer(serializers.Serializer):
    rate = serializers.IntegerField(read_only=True)
    feedback = serializers.CharField(read_only=True)


class RegistrationListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    #schedule = ScheduleReadOnlySerializer(read_only=True)
    schedule_title = serializers.CharField(read_only=True)
    schedule_start_at = serializers.DateTimeField(read_only=True)
    schedule_end_at = serializers.DateTimeField(read_only=True)
    schedule_is_closed = serializers.BooleanField(read_only=True, source="schedule.is_closed")
    event_tags = serializers.CharField(read_only=True)
    event_image = serializers.ImageField(read_only=True, source="schedule.event.main_image")
    event_title = serializers.CharField(read_only=True, source="schedule.event.title")
    event_id = serializers.CharField(read_only=True, source="schedule.event.id")
    is_confirmed = serializers.BooleanField(read_only=True)
    status = serializers.CharField(read_only=True)
    price = serializers.IntegerField(read_only=True)
    card_id = serializers.IntegerField(required=True)
    host_feedback = HostFeedbackReadOnlySerializer(read_only=True)
    # card_name = serializers.CharField(source="payment_log.card_name", read_only=True)
    # card_no = serializers.CharField(source="payment_log.card_no", read_only=True)
    card_name = serializers.SerializerMethodField(read_only=True)
    card_no = serializers.SerializerMethodField(read_only=True)
    #event_replies = EventReplyReadSerializer(read_only=True, many=True)

    def get_card_name(self, instance):
        bk = BillKey.objects.filter(id=instance.card_id).first()
        if bk:
            return bk.card_name
        return None

    def get_card_no(self, instance):
        bk = BillKey.objects.filter(id=instance.card_id).first()
        if bk:
            return bk.card_no
        return None


class RegistrationHostListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    profile = ProfileHostReadOnlySerializer(source="user.profile")
    is_confirmed = serializers.BooleanField(read_only=True)
    status = serializers.CharField(read_only=True)
    price = serializers.IntegerField(read_only=True)
    event_replies = EventReplyReadSerializer(read_only=True, many=True)
    rate = serializers.SerializerMethodField(read_only=True)

    def get_rate(self, instance):
        data = GuestFeedback.objects.filter(guest=instance.user).aggregate(
            overall=Avg('rate')
        )
        return data.get('overall')

class RegistrationSerializer(WritableNestedModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    event_replies = EventReplySerializer(many=True, required=False)
    schedule_detail = ScheduleSerializer(source="schedule", read_only=True)
    event_detail = EventRetrieveSerializer(source='schedule.event', read_only=True)
    promo_code = serializers.CharField(required=False)
    card_id = serializers.IntegerField(required=True)
    # card_name = serializers.CharField(source="payment_log.card_name", read_only=True)
    # card_no = serializers.CharField(source="payment_log.card_no", read_only=True)
    card_name = serializers.SerializerMethodField(read_only=True)
    card_no = serializers.SerializerMethodField(read_only=True)

    def get_card_name(self, instance):
        bk = BillKey.objects.filter(id=instance.card_id).first()
        if bk:
            return bk.card_name
        return None

    def get_card_no(self, instance):
        bk = BillKey.objects.filter(id=instance.card_id).first()
        if bk:
            return bk.card_no
        return None

    class Meta:
        model = Registration
        fields = "__all__"
        read_only_fields = ["is_confirmed", "status"]
        extra_kwargs = {
            'schedule': {'write_only': True},
        }

    def validate_event_replies(self, value):
        event_question_ids = []
        schedule = Schedule.objects.filter(pk=self.initial_data.get('schedule')).first()

        if len(value) != schedule.event.questions.count():
            raise serializers.ValidationError("모든 질문에 대답해야합니다.")

        for v in value:
            # event_question_id = v['event_question']

            # if event_question_id in event_question_ids:
            #     raise serializers.ValidationError("한 질문당 하나의 응답만 할 수 있습니다.")

            # eq = EventQuestion.objects.filter(pk=event_question_id).first()

            # if not eq:
            #     raise serializers.ValidationError("이 질문은 존재하지 않습니다.")
            eq = v['event_question']

            if schedule.event != eq.event:
                raise serializers.ValidationError("이 일정에 대한 질문이 아닙니다.")

            event_question_ids.append(v['event_question'])

        return value

    def validate_schedule(self, value):
        if value.is_closed:
            raise serializers.ValidationError("이 일정은 마감되었습니다.")

        user = self.context['request'].user

        if Registration.objects.filter(schedule=value, user=user).exists():
            raise serializers.ValidationError("이미 이 일정에 신청하셨습니다.")

        return value

class RegistrationConfirmSerializer(serializers.Serializer):
    id = serializers.ListField(child=serializers.IntegerField())


class GuestFeedbackReadOnlySerializer(serializers.Serializer):
    rate = serializers.IntegerField(read_only=True)
    feedback = serializers.CharField(read_only=True)


class ConfirmedRegistrationListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    user_id = serializers.IntegerField(read_only=True, source="user.pk")
    profile = ProfileReadOnlySerializer(read_only=True, source="user.profile")
    schedule_id = serializers.IntegerField(read_only=True, source="schedule.id")
    schedule_title = serializers.CharField(read_only=True)
    schedule_start_at = serializers.DateTimeField(read_only=True)
    schedule_end_at = serializers.DateTimeField(read_only=True)
    guest_feedback = GuestFeedbackReadOnlySerializer(read_only=True)


class GuestFeedbackSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = GuestFeedback
        fields = "__all__"


class HostFeedBackSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = HostFeedback
        fields = "__all__"


class LikedEventReadOnlySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    event_id = serializers.IntegerField(read_only=True, source="event.id")
    event_title = serializers.CharField(read_only=True, source="event.title")
    event_tags = serializers.CharField(read_only=True, source="event.tags")
    event_image = serializers.ImageField(read_only=True, source="event.main_image")
    event_price = serializers.IntegerField(read_only=True, source="event.last_schedule_price")
    event_type = serializers.CharField(read_only=True, source="event.event_type")


class LikedEventSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    class Meta:
        model = LikedEvent
        fields = "__all__"

    def validate_event(self, value):
        if not value.is_verified:
            raise serializers.ValidationError("이 모임을 찜할 수 없습니다.")
        return value


class MagazineListSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    title = serializers.CharField(read_only=True)
    subtitle = serializers.CharField(read_only=True)
    image = serializers.ImageField(read_only=True)
    created_at = serializers.DateTimeField(read_only=True)
    updated_at = serializers.DateTimeField(read_only=True)


class MegazineRetrieveSerializer(MagazineListSerializer):
    content = serializers.CharField(read_only=True)
    keyword = serializers.CharField(read_only=True)
    #events = EventListSerializer(many=True, read_only=True)


class MainMagazineSerializer(serializers.Serializer):
    magazine = MagazineListSerializer(read_only=True)
