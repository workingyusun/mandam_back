import datetime
import logging

from django.db.models import Max, Min, Count, Q, F, Avg
from django.db.models.expressions import Case, When
from django.db.models import OuterRef, Subquery
from django.db.models.functions import Coalesce
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page

from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.settings import api_settings

from apps.accounts.serializers import FeedbackReadOnlySerializer
from apps.core.models import BillKey
from apps.core.permissions import IsOwnerOrReadOnly
from apps.core.payment_utils import payment
from apps.core.utils import make_error_message, get_now, get_kt
from apps.core.alimtalk import make_alimtalk, bulk_make_alimtalk_with_same_message

from .models import Event, Schedule, Registration, TopSearched, FeaturedTag, MainPage, PromoCode, PromoCodeLog
from .models import LikedEvent, GuestFeedback, MainBanner, Magazine, MainMagazine
from .serializers import EventSerializer, ScheduleSerializer, RegistrationSerializer, EventListSerializer
from .serializers import TopSearchedSerializer, FeaturedTagSerialzier, FeaturedTagSerialzier, EventRetrieveSerializer
from .serializers import EventMeSerialzier, MainPageSerializer, EventUpdateSerializer, ScheduleReadOnlySerializer
from .serializers import RegistrationListSerializer, RegistrationHostListSerializer, RegistrationConfirmSerializer
from .serializers import ConfirmedRegistrationListSerializer, GuestFeedbackSerializer, HostFeedBackSerializer
from .serializers import PromoCodeReadOnlySerializer, LikedEventReadOnlySerializer, LikedEventSerializer, MainMagazineSerializer
from .serializers import ScheduleRetrieveSerializer, MainBannerSerializer, MagazineListSerializer, MegazineRetrieveSerializer
from .paginations import EventPageNumberPagination


logging.basicConfig(level=logging.WARNING)
logger = logging.getLogger(__name__)


class MainMagazineViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = MainMagazine.objects.all()
    serializer_class = MainMagazineSerializer


class MagazineViewSet(viewsets.GenericViewSet, 
                      mixins.ListModelMixin, 
                      mixins.RetrieveModelMixin):
    queryset = Magazine.objects.all()
    serializer_class = MagazineListSerializer

    def get_queryset(self):
        if self.request.query_params.get('is_main', None) == "true":
            queryset = self.queryset.filter(is_main=True)
        else:
            queryset = self.queryset
        return queryset.order_by('-created_at')

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())

        self.pagination_class.page_size = 6

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = MegazineRetrieveSerializer(instance)
        return Response(serializer.data)


class MainBannerViewset(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = MainBanner.objects.all().order_by("-created_at")
    serializer_class = MainBannerSerializer


class MainPageViewset(viewsets.GenericViewSet, mixins.ListModelMixin):
    queryset = MainPage.objects.all().order_by("-created_at")
    serializer_class = MainPageSerializer


class TopSearchedViewSet(viewsets.GenericViewSet,
                         mixins.ListModelMixin):
    queryset = TopSearched.objects.all().order_by('-created_at')
    serializer_class = TopSearchedSerializer


class FeaturedTagViewSet(viewsets.GenericViewSet,
                         mixins.ListModelMixin):
    queryset = FeaturedTag.objects.all().order_by("-created_at")
    serializer_class = FeaturedTagSerialzier


class LikedEventViewSet(viewsets.GenericViewSet,
                        mixins.ListModelMixin,
                        mixins.CreateModelMixin,
                        mixins.DestroyModelMixin):
    queryset = LikedEvent.objects.all().prefetch_related('event__schedules')
    serializer_class = LikedEventSerializer
    permission_classes = [IsAuthenticated, IsOwnerOrReadOnly, ]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        queryset = queryset.filter(user=request.user).order_by('-created_at')
        # newest = Schedule.objects.filter(event=OuterRef('pk')).order_by('-created_at')
        # queryset = queryset.annotate(price=Subquery(newest.values('price')[:1])).order_by('-created_at')

        self.pagination_class.page_size = 8
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = LikedEventReadOnlySerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = LikedEventReadOnlySerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        event = serializer.validated_data.get("event")

        if LikedEvent.objects.filter(user=request.user, event=event).exists():
            return Response(make_error_message(100014), status=status.HTTP_400_BAD_REQUEST)

        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class PromoCodeViewSet(viewsets.GenericViewSet):
    queryset = PromoCode.objects.all()
    serializer_class = PromoCodeReadOnlySerializer
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        code = kwargs.get('pk', None)
        now = get_now()
        instance = self.get_queryset().filter(code=code, start_at__lte=now, end_at__gte=now).first()

        if not instance:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if PromoCodeLog.objects.filter(user=request.user, promo_code=instance).exists():
            return Response(make_error_message(100013),status=status.HTTP_400_BAD_REQUEST)

        serializer = self.get_serializer(instance)
        return Response(serializer.data)


class EventViewSet(viewsets.GenericViewSet,
                   mixins.CreateModelMixin,
                   mixins.UpdateModelMixin):
    queryset = Event.objects.all()
    serializer_class = EventSerializer
    pagination_class = EventPageNumberPagination
    permission_classes = [IsOwnerOrReadOnly, ]

    def get_queryset(self):
        queryset = self.queryset

        order = self.request.query_params.get('order', None)
        q = self.request.query_params.get('q', None)

        now = get_now()

        # 일정 없는 것 뺴고
        queryset = queryset.exclude(schedules__isnull=True)

        # 일정의 최고 값이 현재보다 늦은 이벤트들 빼고
        queryset = queryset.annotate(Max("schedules__start_at")).filter(schedules__start_at__max__gte=now)

        # 확정한것만 있는 것 빼고
        num_of_not_closed_schedule = Count('schedules', filter=Q(schedules__is_closed=False))   
        queryset = queryset.annotate(
            num_of_not_closed_schedule=num_of_not_closed_schedule
        ).exclude(event_type="S", num_of_not_closed_schedule__lt=1)

        # 직접선택 8일 안으로 들어온건 안 보여줌
        queryset = queryset.exclude(event_type="S", schedules__start_at__max__lte=now+datetime.timedelta(days=7))

        if q:
            tag_filtered = queryset.filter(tags__icontains=q)
            title_filtered = queryset.filter(title__icontains=q)
            queryset = tag_filtered | title_filtered

        if order == "upcoming":
            fcfs_queryset = queryset.filter(event_type="F").annotate(
                min_start_at=Min(Case(When(
                    schedules__start_at__gte=now+datetime.timedelta(days=1), then=F("schedules__start_at")
                    )))
            )
            select_queryset = queryset.filter(event_type="S").annotate(
                min_start_at=Min(Case(When(
                    schedules__start_at__gte=now+datetime.timedelta(days=7), then=F("schedules__start_at")
                    )))
            )
            queryset = fcfs_queryset | select_queryset
            queryset = queryset.order_by("min_start_at")
        else:
            queryset = queryset.order_by("-created_at")
        return queryset

    # @method_decorator(cache_page(60))
    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset()).filter(is_verified=True).prefetch_related("liked_events")

        if request.user.is_authenticated:
            user_pk = request.user.pk
        else:
            # make this null
            user_pk = 0

        liked_event = LikedEvent.objects.filter(event=OuterRef('id'), user__pk=user_pk)
        queryset = queryset.annotate(liked_event=Subquery(liked_event.values('id')[:1]))
        queryset = queryset.order_by('-created_at')

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = EventListSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = EventListSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        if not pk:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        instance = Event.objects.filter(pk=pk).first()

        if not instance:
            return Response(status=status.HTTP_404_NOT_FOUND)

        user = request.user

        if user != instance.user:
            if instance.is_draft or not instance.is_verified:
                return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = EventRetrieveSerializer(instance)
        data = serializer.data

        liked_event = None
        if request.user.is_authenticated:
            liked_event = LikedEvent.objects.filter(event=instance, user=request.user).first()
            if liked_event:
                liked_event = liked_event.id

        data['liked_event'] = liked_event
        return Response(data)

    def update(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if not pk:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        instance = Event.objects.filter(pk=pk).first()

        if not instance:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # 모임 심사 거절 되면 변경 불가
        if instance.is_declined:
            return Response(make_error_message(200000), status=status.HTTP_400_BAD_REQUEST)

        # 심사 기간 중 변경 불가
        if not instance.is_draft and not instance.is_verified:
            return Response(make_error_message(200001), status=status.HTTP_400_BAD_REQUEST)

        # Block False to True
        is_draft = request.data.get('is_draft', False)
        if not instance.is_draft and (is_draft == "true"):
            return Response(make_error_message(200002), status=status.HTTP_400_BAD_REQUEST)

        # Block to update event_type
        event_type = request.data.get("event_type", None)
        if event_type:
            return Response(make_error_message(200003), status=status.HTTP_400_BAD_REQUEST)

        partial = kwargs.pop('partial', False)
        serializer_context = self.get_serializer_context()
        serializer = EventUpdateSerializer(instance, data=request.data, partial=partial, context=serializer_context)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    @action(methods=['get'], detail=False, permission_classes=[IsAuthenticated])
    def me(self, request, *args, **kwargs):
        user = request.user

        if not user.profile.is_host:
            return Response(status=status.HTTP_403_FORBIDDEN)

        queryset = self.filter_queryset(Event.objects.filter(user=request.user))

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = EventMeSerialzier(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = EventMeSerialzier(queryset, many=True)
        return Response(serializer.data)

    @action(methods=['post'], detail=True, serializer_class=ScheduleSerializer, permission_classes=[IsAuthenticated])
    def schedule(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if not pk:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        instance = Event.objects.filter(pk=pk).first()

        if not instance:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # 모임 심사 거절 되면 일정 못만듬
        if instance.is_declined:
            return Response(make_error_message(200000), status=status.HTTP_400_BAD_REQUEST)

        # 심사 기간 중 일정 못 만듬
        if not instance.is_draft and not instance.is_verified:
            return Response(make_error_message(200001), status=status.HTTP_400_BAD_REQUEST)

        user = request.user

        if user != instance.user:
            if instance.is_draft or not instance.is_verified:
                return Response(status=status.HTTP_404_NOT_FOUND)

        request.data.update({'event': instance.pk})
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    @action(
        methods=["get"], detail=True,
        serializer_class=ScheduleReadOnlySerializer)
    def schedules(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if not pk:
            return Response(status=status.HTTP_400_BAD_REQUEST)

        instance = Event.objects.filter(pk=pk).first()

        if not instance:
            return Response(status=status.HTTP_404_NOT_FOUND)

        is_closed = request.query_params.get('is_closed')
        now = get_now()

        if is_closed == "false":
            schedules = instance.schedules.filter(start_at__gt=now)
        elif is_closed == "true":
            schedules = instance.schedules.filter(start_at__lte=now)
        else:
            schedules = instance.schedules.all()

        schedules = schedules.order_by('-created_at')
        page = self.paginate_queryset(schedules)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(schedules, many=True)
        return Response(serializer.data)


class ScheduleViewSet(viewsets.GenericViewSet, mixins.UpdateModelMixin, 
                      mixins.RetrieveModelMixin, mixins.DestroyModelMixin):
    queryset = Schedule.objects.all()
    serializer_class = ScheduleSerializer
    permission_classes = [IsAuthenticated]

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = ScheduleRetrieveSerializer(instance)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if request.user != instance.event.user:
            return Response(status=status.HTTP_403_FORBIDDEN)
        if instance.registrations.exists():
            return Response(make_error_message(200004), status=status.HTTP_400_BAD_REQUEST)
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()

        if instance.event.user != request.user:
            return Response(status=status.HTTP_404_NOT_FOUND)
        if instance.registrations.exists():
            return Response(status=status.HTTP_400_BAD_REQUEST, data=make_error_message(100001))
        if instance.is_closed:
            return Response(status=status.HTTP_400_BAD_REQUEST, data=make_error_message(100002))

        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    @action(
        methods=["get"], detail=True,
        serializer_class=RegistrationHostListSerializer)
    def registrations(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        schedule = Schedule.objects.filter(pk=pk).first()

        if not schedule:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if schedule.event.user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        queryset = schedule.registrations.all().order_by("-created_at")

        self.pagination_class.page_size = 8
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(methods=["GET"], detail=True)
    def guest_rate(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        schedule = Schedule.objects.filter(pk=pk).first()

        if not schedule:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if schedule.event.user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        queryset = GuestFeedback.objects.filter(
            id__in=schedule.registrations.all().values('id')
        )

        data = queryset.aggregate(overall=Avg('rate'))

        return Response(data, status=status.HTTP_200_OK)

    @action(
        methods=["GET"], detail=True,
        serializer_class=FeedbackReadOnlySerializer
    )
    def guest_reviews(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        schedule = Schedule.objects.filter(pk=pk).first()

        if not schedule:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if schedule.event.user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        queryset = GuestFeedback.objects.filter(
            id__in=schedule.registrations.all().values('id')
        ).order_by('-created_at')

        self.pagination_class.page_size = 5

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


    @action(
        methods=["patch"], detail=True,
        serializer_class=RegistrationConfirmSerializer
    )
    def confirm(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        schedule = Schedule.objects.filter(pk=pk).first()

        if not schedule:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if schedule.event.user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        if schedule.event_type != "S":
            return Response(make_error_message(100008), status=status.HTTP_400_BAD_REQUEST)

        if schedule.is_closed:
            return Response(make_error_message(100010), status=status.HTTP_400_BAD_REQUEST)

        if schedule.registrations.exclude(status='ready').exists():
            return Response(make_error_message(100010), status=status.HTTP_400_BAD_REQUEST)

        now = get_now()

        if schedule.start_at <= (now + datetime.timedelta(days=8)):
            return Response(make_error_message(100011), status=status.HTTP_400_BAD_REQUEST)

        pk_list = request.data.get('id')

        if not len(pk_list):
            return Response(make_error_message(100012), status=status.HTTP_400_BAD_REQUEST)

        if len(pk_list) > schedule.quota:
            return Response(make_error_message(100009), status=status.HTTP_400_BAD_REQUEST)

        # confirmed
        regis = schedule.registrations.filter(pk__in=pk_list)
        regi_list = []

        for regi in regis:
            regi.status = "payment_ready"
            regi_list.append(regi)

        Registration.objects.bulk_update(regi_list, ["status"])

        # dropped
        failed_regis = schedule.registrations.exclude(pk__in=pk_list)
        failed_regi_list = []

        for regi in failed_regis:
            regi.status = "dropped"
            failed_regi_list.append(regi)

        Registration.objects.bulk_update(failed_regi_list, ["status"])

        schedule.is_closed = True
        schedule.save()
        serializer = ScheduleRetrieveSerializer(schedule)

        #failed_regis = schedule.registrations.exclude(pk__in=pk_list)
        to_list = []
        for regi in failed_regis:
            to_list.append(regi.user.phonenumber.phone_number)
        if to_list:
            alimtalk_kwargs = {
                "event_name": schedule.event.title
            }
            bulk_make_alimtalk_with_same_message(
                to_list=to_list,
                template_code="MANDAM0005",
                **alimtalk_kwargs
            )
        return Response(serializer.data, status=status.HTTP_200_OK)

    @action(
        methods=["GET"], detail=True,
        serializer_class=ConfirmedRegistrationListSerializer
    )
    def confirmed_registrations(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        schedule = Schedule.objects.filter(pk=pk).first()

        if not schedule:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if schedule.event.user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        queryset = schedule.registrations.filter(is_confirmed=True).select_related('schedule').order_by("-created_at")
        self.pagination_class.page_size = 8
        page = self.paginate_queryset(queryset)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    def get_success_headers(self, data):
        try:
            return {'Location': str(data[api_settings.URL_FIELD_NAME])}
        except (TypeError, KeyError):
            return {}

    @action(
        methods=["POST"], detail=True,
        serializer_class=GuestFeedbackSerializer
    )
    def guest_feedback(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        schedule = Schedule.objects.filter(pk=pk).first()

        if not schedule:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if schedule.event.user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        registration = serializer.validated_data.get('registration')

        if not registration.is_confirmed:
            return Response(status=status.HTTP_403_FORBIDDEN)

        if registration.schedule.pk != schedule.pk:
            return Response(status=status.HTTP_403_FORBIDDEN)

        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class RegistrationViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, 
                          mixins.RetrieveModelMixin, mixins.DestroyModelMixin):
    queryset = Registration.objects.all()
    serializer_class = RegistrationSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return super().get_queryset().filter(user=self.request.user).select_related('schedule__event').order_by("-created_at")

    def perform_create(self, serializer):
        return serializer.save()

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        self.pagination_class.page_size = 8

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = RegistrationListSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = RegistrationListSerializer(queryset, many=True)
        return Response(serializer.data)

    @action(
        methods=["POST"], detail=True,
        serializer_class=HostFeedBackSerializer
    )
    def host_feedback(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)
        regi = Registration.objects.filter(pk=pk).first()

        if not regi:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if regi.user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        if not regi.is_confirmed:
            return Response(status=status.HTTP_403_FORBIDDEN)

        data = request.data.copy()
        data['registration'] = regi.pk

        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)

        serializer.save()
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()

        if instance.user != request.user:
            return Response(status=status.HTTP_403_FORBIDDEN)

        if instance.status != "ready":
            return Response(make_error_message(200005), status=status.HTTP_400_BAD_REQUEST)

        event = instance.schedule.event

        if event.event_type != "S":
            return Response(make_error_message(200006), status=status.HTTP_400_BAD_REQUEST)

        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        code = serializer.validated_data.pop('promo_code', None)
        card_id = serializer.validated_data.pop('card_id', None)
        card = BillKey.objects.filter(id=card_id, user=request.user).first()

        if not card:
            return Response(make_error_message(100015), status=status.HTTP_400_BAD_REQUEST)

        if code:
            now = get_now()
            promo_code = PromoCode.objects.filter(code=code, start_at__lte=now, end_at__gte=now).first()
            # PROMO CODE 가 없을 경우
            if not promo_code:
                return Response(make_error_message(100005), status=status.HTTP_400_BAD_REQUEST)
            # PROMO CODE가 사용됐을경우
            if PromoCodeLog.objects.filter(promo_code=promo_code, user=request.user).exists():
                return Response(make_error_message(100006), status=status.HTTP_400_BAD_REQUEST)

        schedule = serializer.validated_data.get('schedule')
        event = schedule.event

        if code:
            if schedule.price < promo_code.min_price:
                return Response(make_error_message(100007), status=status.HTTP_400_BAD_REQUEST)

        if event.event_type == "F":
            registered_count = schedule.registrations.filter(
                Q(status="payment_success") | Q(status="payment_ready")).count()
            if schedule.quota <= registered_count:
                return Response(make_error_message(100004), status=status.HTTP_400_BAD_REQUEST)
            instance = self.perform_create(serializer) # payment_ready
            instance.card_id = card_id
            # PROMO CODE LOGIC START
            if code:
                price = instance.price - promo_code.discount

                if price < promo_code.min_price:
                    price = promo_code.min_price

                instance.price = price
            # PROMO CODE LOGIC END
            is_succeed = False
            try:
                # 결제 시작
                # payment start
                payment_log, is_succeed = payment(
                    request.user,
                    card.bid,
                    schedule.title,
                    instance.price
                )
                instance.payment_log = payment_log
                if is_succeed:
                    instance.status = "payment_success"
                else:
                    instance.status = "payment_failed"
                instance.save()
                # payment end
            except Exception as e:
                # make a log for exception
                instance.status = "payment_failed"
                instance.save()
                logger.critical(e)
                is_succeed = False

            registered_count = schedule.registrations.filter(
                Q(status="payment_success") | Q(status="payment_ready")
            ).count()

            # 쿼타 차면 문 닫기
            if schedule.quota <= registered_count:
                schedule.is_closed = True
                schedule.save()

            alimtalk_kwargs = {
                "event_name": event.title,
                "schedule_name": schedule.title,
                "schedule_datetime": get_kt(schedule.start_at).strftime("%Y/%m/%d %H:%M:%S"),
                "purchase_date": get_kt(instance.updated_at).strftime("%Y/%m/%d %H:%M:%S"),
                "price": instance.price,
                "payment_method": f"{card.card_name} {card.card_no}"
            }

            if is_succeed:
                template_code = "MANDAM0001"
            else:
                template_code = "MANDAM0002"

            make_alimtalk(
                to=instance.user.phonenumber.phone_number,
                template_code=template_code,
                **alimtalk_kwargs
            )

        elif event.event_type == "S":
            instance = self.perform_create(serializer)
            # PROMO CODE LOGIC START
            if code:
                price = instance.price - promo_code.discount

                if price < promo_code.min_price:
                    price = promo_code.min_price

                instance.price = price
                instance.card_id = card_id
                instance.save()
            # PROMO CODE LOGIC END
            alimtalk_kwargs = {
                "event_name": event.title,
                "schedule_name": schedule.title,
                "schedule_datetime": get_kt(schedule.start_at).strftime("%Y/%m/%d %H:%M:%S")
            }
            make_alimtalk(
                to=instance.user.phonenumber.phone_number,
                template_code="MANDAM0003",
                **alimtalk_kwargs
            )
        else:
            return Response(make_error_message(100003), status=status.HTTP_400_BAD_REQUEST)

        if code:
            PromoCodeLog.objects.create(promo_code=promo_code, user=request.user, schedule=schedule)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


# TODO SELECT 결제 만들기 async (CRON?)
# 결제 성공 -> is_confirmed true, 결제 성공으로 바꾸기
# 실패 -> 결제 실패로 바꾸기
