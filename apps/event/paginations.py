from apps.core.paginations import OnlyPageNumberPagination


class EventPageNumberPagination(OnlyPageNumberPagination):
    page_size = 12
