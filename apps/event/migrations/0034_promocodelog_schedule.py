# Generated by Django 2.2.12 on 2020-05-31 12:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0033_auto_20200531_1146'),
    ]

    operations = [
        migrations.AddField(
            model_name='promocodelog',
            name='schedule',
            field=models.ForeignKey(default=19, on_delete=django.db.models.deletion.CASCADE, to='event.Schedule'),
            preserve_default=False,
        ),
    ]
