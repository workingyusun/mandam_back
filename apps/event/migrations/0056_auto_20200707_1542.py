# Generated by Django 2.2.12 on 2020-07-07 15:42

import ckeditor.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0055_mainbanner_link'),
    ]

    operations = [
        migrations.AlterField(
            model_name='magazine',
            name='content',
            field=ckeditor.fields.RichTextField(),
        ),
    ]
