# Generated by Django 2.2.12 on 2020-05-02 11:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0002_auto_20200501_2057'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='tags',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
