# Generated by Django 2.2.12 on 2020-05-01 20:57

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='is_draft',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='eventimage',
            name='image',
            field=models.ImageField(upload_to='uploads/event/%Y/%m/%d/'),
        ),
    ]
