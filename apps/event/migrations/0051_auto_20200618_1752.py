# Generated by Django 2.2.12 on 2020-06-18 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0050_magazine_events'),
    ]

    operations = [
        migrations.AlterField(
            model_name='magazine',
            name='events',
            field=models.ManyToManyField(related_name='magazines', to='event.Event'),
        ),
    ]
