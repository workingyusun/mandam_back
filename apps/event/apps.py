from django.apps import AppConfig


class EventConfig(AppConfig):
    name = 'apps.event'
    verbose_name = "서비스 관리"
