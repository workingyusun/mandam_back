from django.contrib import admin
from django.contrib.auth import admin as auth_admin
from django.contrib.auth import get_user_model

from .models import Profile, Interest, PhoneNumber, HostApplication, CheckPlusLog


@admin.register(PhoneNumber)
class PhoneNumberAdmin(admin.ModelAdmin):
    list_display = ['user_id', 'email', 'phone_number', ]
    search_fields = ["phone_number", 'user__email']
    raw_id_fields = ('user',)


@admin.register(Profile)
class ProfileAdmin(admin.ModelAdmin):
    list_display = ['user', 'email', 'name', 'nickname', 'gender', 'is_host', ]
    raw_id_fields = ('user', )
    search_fields = ["name", 'nickname', 'user__email']


@admin.register(HostApplication)
class HostApplicationAdmin(admin.ModelAdmin):
    list_display = ['user_id', 'email', 'is_corporate', 'is_verified', ]
    raw_id_fields = ('user',)
    search_fields = ['user__email']


@admin.register(Interest)
class InterestAdmin(admin.ModelAdmin):
    list_display = ['id', 'name']


@admin.register(CheckPlusLog)
class CheckPlusLogAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'phone_number', 'result', 'reqseq', ]
    search_fields = ['name', 'phone_number']
