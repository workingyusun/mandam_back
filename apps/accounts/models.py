from django.conf import settings
from django.db import models
from django.utils.translation import gettext as _

from .utils import year_choices, current_year


class Interest(models.Model):
    name = models.CharField(max_length=30)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = '관심사'
        verbose_name_plural = '관심사'


class Profile(models.Model):
    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    name = models.CharField("이름", max_length=10, blank=True, default="")
    nickname = models.CharField("닉네임", max_length=10, blank=True, default="")
    image = models.ImageField("프로필이미지", upload_to='uploads/profile/%Y/%m/%d/', blank=True)
    birth_year = models.IntegerField("생일", choices=year_choices(), default=current_year)
    gender = models.CharField("성별", max_length=1, choices=GENDER_CHOICES)
    interests = models.ManyToManyField(Interest, blank=True)
    self_text = models.TextField("자기소개", blank=True, default="")
    instagram_id = models.CharField("인스타그램", max_length=50, blank=True, null=True, default=None, unique=True)
    facebook_id = models.CharField("페이스북", max_length=50, blank=True, null=True, default=None, unique=True)
    twitter_id = models.CharField("트위터", max_length=50, blank=True, null=True, default=None, unique=True)
    is_host = models.BooleanField("모임 주최 여부", default=False)
    is_marketing_approved = models.BooleanField("마케팅 동의 여부", default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.email

    @property
    def email(self):
        return self.user.email

    def save(self, *args, **kwargs):

        if self.facebook_id == "":
            self.facebook_id = None

        if self.twitter_id == "":
            self.twitter_id = None

        if self.instagram_id == "":
            self.instagram_id = None

        super(Profile, self).save(*args, **kwargs)
        
    class Meta:
        verbose_name = '프로필'
        verbose_name_plural = '프로필'


class PhoneNumber(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    phone_number = models.CharField(verbose_name='phone_number', max_length=11, unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'{self.user.email}: {self.phone_number}'

    @property
    def email(self):
        return self.user.email
    
    class Meta:
        verbose_name = '휴대폰 번호'
        verbose_name_plural = '휴대폰 번호'


class HostApplication(models.Model):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE
    )
    is_corporate = models.BooleanField("콜라보레이션", default=False)
    certificate_image = models.ImageField("신분증 사본", upload_to='uploads/host_application/certificate_image/%Y/%m/%d/')
    bank = models.CharField("입금 은행", max_length=15)
    account_number = models.CharField("계좌번호", max_length=50)
    account_image = models.ImageField("통장사본", upload_to='uploads/host_application/account_image/%Y/%m/%d/')
    is_verified = models.BooleanField("사용자 인증", default=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.user.email

    @property
    def email(self):
        return self.user.email

    def save(self, *args, **kwargs):
        super(HostApplication, self).save(*args, **kwargs)
        profile = self.user.profile
        profile.is_host = self.is_verified
        profile.save()

    @property
    def username(self):
        return self.user.username

    class Meta:
        verbose_name = '정산 정보'
        verbose_name_plural = '정산 정보'


class CheckPlusLog(models.Model):
    reqseq = models.TextField()
    name = models.CharField(max_length=100, blank=True, default="")
    phone_number = models.CharField(max_length=30, blank=True, default="")
    gender = models.PositiveSmallIntegerField(default=1)
    result = models.CharField(max_length=100, blank=True, default="")
    birthdate = models.CharField(max_length=100, blank=True, default="")
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.reqseq

    class Meta:
        verbose_name = 'NICE인증정보'
        verbose_name_plural = 'NICE인증정보'
