from django.db.models.signals import m2m_changed
from django.core.exceptions import ValidationError

from .models import Profile


def interests_changed(sender, **kwargs):
    if kwargs['instance'].interests.count() > 5:
        raise ValidationError("최대 5개까지 선택 가능합니다.")


m2m_changed.connect(interests_changed, sender=Profile.interests.through)
