import os, subprocess, re, base64, sys, environ
from django.shortcuts import render
from django.http import JsonResponse
from django.http import HttpResponseRedirect
from requests import Response

from config.settings.loader import load_env
from apps.core.utils import get_back_url, get_front_url
from .models import CheckPlusLog

sitecode = "BN749"
sitepasswd = "fhjIa2bB1J9k"
# cb_encode_path='C:/Django/vezt3/CPClient'
cb_encode_path = "./CPClient_mac" if load_env("IS_MAC", False, cast=bool) else "./CPClient_64bit"
#TODO HOST
url_nice = 'https://nice.checkplus.co.kr/CheckPlusSafeModel/checkplus.cb?m=checkplusSerivce&EncodeData='
url_session = '&param_r1='

# def checkplus_test(request):
#     encode_data = request.GET.get("EncodeData")
#     return JsonResponse({"url": f"http://localhost:8000/api/v1/accounts/checkplus_success/?EncodeData={encode_data}&REQ_SEQ="})

# 인증결과 데이터 추출 함수
def GetValue(plaindata, key):
    value = ''
    keyIndex = -1
    valLen = 0

    # 복호화 데이터 분할
    arrData = plaindata.split(':')
    cnt = len(arrData)
    for i in range(cnt):
        item = arrData[i]
        itemKey = re.sub('[\d]+$', '', item)

        # 키값 검색
        if itemKey == key:
            keyIndex = i

            # 데이터 길이값 추출
            valLen = int(item.replace(key, '', 1))

            if key != 'NAME':
                # 실제 데이터 추출
                value = arrData[keyIndex + 1][:valLen]
            else:
                # 이름 데이터 추출 (한글 깨짐 대응)
                value = re.sub('[\d]+$', '', arrData[keyIndex + 1])
            break

    return value


#####################################################################################
#    페이지명 : 체크플러스 - 메인 호출 페이지
#####################################################################################
def checkplus_main(request):
    result_url = f"{get_back_url()}/api/v1/accounts/checkplus_success/"
    #result_url = "http://localhost:8000/api/v1/accounts/checkplus_success/"
    # 팝업화면 설정
    authtype = 'M'  # 인증타입 (공백:기본 선택화면, X:공인인증서, M:핸드폰, C:카드)
    popgubun = 'N'  # 취소버튼 (Y:있음, N:없음)
    is_mobile = request.GET.get('is_mobile', False)

    if is_mobile:
        customize = "Mobile"  # 화면타입 (공백:PC페이지, Mobile:모바일페이지)
    else:
        customize = ""

    gender = ''  # 성별설정 (공백:기본 선택화면, 0:여자, 1:남자)
    reqseq = ''
    enc_data = ''
    returnMsg = ''
    # 요청번호 생성
    try:
        reqseq = subprocess.run([cb_encode_path, 'SEQ', sitecode], capture_output=True, encoding='euc-kr').stdout
    except subprocess.CalledProcessError as e:
        reqseq = e.output.decode('euc-kr')
        print('cmd:', e.cmd, '\n output:', e.output)
    finally:
        print('reqseq:', reqseq)

    plaindata = '7:REQ_SEQ' + str(len(reqseq)) + ':' + reqseq + '8:SITECODE' + str(
        len(sitecode)) + ':' + sitecode + '9:AUTH_TYPE' + str(len(authtype)) + ':' + authtype + '7:RTN_URL' + str(
        len(result_url)) + ':' + result_url + '7:ERR_URL' + str(len(result_url)) + ':' + result_url + '11:POPUP_GUBUN' + str(
        len(popgubun)) + ':' + popgubun + '9:CUSTOMIZE' + str(len(customize)) + ':' + customize + '6:GENDER' + str(
        len(gender)) + ':' + gender

    try:
        enc_data = subprocess.run([cb_encode_path, 'ENC', sitecode, sitepasswd, plaindata], capture_output=True,
                                  encoding='euc-kr').stdout
    except subprocess.CalledProcessError as e:
        enc_data = e.output.decode('euc-kr')
        print('cmd:', e.cmd, '\n output:\n', e.output)

    return_url = url_nice + enc_data + url_session
    return JsonResponse({"return_url": return_url, "reqseq": reqseq})


#####################################################################################
#    페이지명 : 체크플러스 - 성공 결과 페이지
#####################################################################################
def checkplus_success(request):
    try:
        reqseq = ''
        enc_data = ''
        plaindata = ''
        returnMsg = ''
        ciphertime = ''
        context = {}
        result = ''
        requestnumber = ''  # 요청번호
        responsenumber = ''  # 본인인증 응답코드 (응답코드 문서 참조)
        authtype = ''  # 인증수단 (M:휴대폰, c:카드, X:인증서, P:삼성패스)
        name = ''  # 이름 (EUC-KR)
        utfname = ''  # 이름 (UTF-8, URL인코딩)
        birthdate = ''  # 생년월일 (YYYYMMDD)
        gender = ''  # 성별 코드 (0:여성, 1:남성)
        nationalinfo = ''  # 내/외국인 정보 (0:내국인, 1:외국인)
        dupinfo = ''  # 중복가입확인값 (64Byte, 개인식별값, DI:Duplicate Info)
        conninfo = ''  # 연계정보확인값 (88Byte, 개인식별값, CI:Connecting Info)
        mobileno = ''  # 휴대폰번호
        mobileco = ''  # 통신사 (가이드 참조)

        session = request.session


        try:
            # GET 요청 처리
            if request.method == 'GET':
                print('checkplus_success:GET')
                enc_data = request.GET.get('EncodeData', '')
            # POST 요청 처리
            else:
                print('checkplus_success:POST')
                enc_data = request.POST.get('EncodeData', '')
        except:
            print("ERR:", sys.exc_info()[0])
        ################################### 문자열 점검 ######################################
        errChars = re.findall('[^0-9a-zA-Z+/=]', enc_data)
        if len(re.findall('[^0-9a-zA-Z+/=]', enc_data)) > 0:
            print("errChars=", errChars)
            return JsonResponse({"error": '문자열오류: 입력값 확인이 필요합니다'})
        if (base64.b64encode(base64.b64decode(enc_data))).decode() != enc_data:
            return JsonResponse({"error": '변환오류: 입력값 확인이 필요합니다'})
        #####################################################################################
        # try:
        #     reqseq = request.get['REQ_SEQ']
        # except Exception as e:
        #     returnMsg = "세션값이 존재하지 않습니다."

        if enc_data != '':
            # 인증결과 암호화 데이터 복호화 처리
            try:
                # 파이썬 버전이 3.5 미만인 경우 check_output 함수 이용
                #  plaindata = subprocess.check_output([cb_encode_path, 'DEC', sitecode, sitepasswd, enc_data])
                plaindata = subprocess.run([cb_encode_path, 'DEC', sitecode, sitepasswd, enc_data], capture_output=True,
                                           encoding='euc-kr').stdout
            except subprocess.CalledProcessError as e:
                # check_output 함수 이용하는 경우 1 이외의 결과는 에러로 처리됨
                plaindata = e.output.decode('euc-kr')
                print('cmd:', e.cmd, '\n output:\n', e.output)

        else:
            returnMsg = '처리할 암호화 데이타가 없습니다.'

        # 복호화 처리결과 코드 확인
        if plaindata == -1:
            returnMsg = '암/복호화 시스템 오류'
        elif plaindata == -4:
            returnMsg = '복호화 처리 오류'
        elif plaindata == -5:
            returnMsg = 'HASH값 불일치 - 복호화 데이터는 리턴됨'
        elif plaindata == -6:
            returnMsg = '복호화 데이터 오류'
        elif plaindata == -9:
            returnMsg = '입력값 오류'
        elif plaindata == -12:
            returnMsg = '사이트 비밀번호 오류'
        else:
            # 요청번호 추출
            requestnumber = GetValue(plaindata, 'REQ_SEQ')

        # if reqseq == requestnumber:
        try:
            ciphertime = subprocess.run([cb_encode_path, 'CTS', sitecode, sitepasswd, enc_data], capture_output=True, encoding='euc-kr').stdout
        except subprocess.CalledProcessError as e:
            ciphertime = e.output.decode('euc-kr')
            print('cmd:', e.cmd, '\n output:', e.output)
        finally:
            print('ciphertime:', ciphertime)
        # else:
        #     returnMsg = '세션 불일치 오류'

        errcode = GetValue(plaindata, 'ERR_CODE')
        print('ERR_CODE :', errcode)
        context = {}
        # 인증결과 복호화 시간 확인
        if ciphertime != '':
            returnMsg = "사용자 인증에 성공하였습니다."
            cpl = CheckPlusLog(
                reqseq=GetValue(plaindata, 'REQ_SEQ'),
                name=GetValue(plaindata, 'NAME'),
                phone_number=GetValue(plaindata, 'MOBILE_NO'),
                gender=GetValue(plaindata, 'GENDER'),
                result="success",
                birthdate=GetValue(plaindata, 'BIRTHDATE'),
            )
            cpl.save()
            context = {
                'result' : "success",
                'req_seq': GetValue(plaindata, 'REQ_SEQ'),
                'name': GetValue(plaindata, 'NAME'),
                'gender': GetValue(plaindata, 'GENDER'),
                'birthdate': GetValue(plaindata, 'BIRTHDATE'),
                'phone': GetValue(plaindata, 'MOBILE_NO'),
                'returnMsg': returnMsg,
                'dupinfo' : GetValue(plaindata, 'DI'),
                'conninfo' : GetValue(plaindata, 'CI'),
            }
        #return JsonResponse(context)
        return HttpResponseRedirect(f"{get_front_url()}/api/checkpass?name={context.get('name')}&phone={context.get('phone')}&birth_year={context.get('birthdate')[:4]}&reqseq={context.get('req_seq')}")
        #return render(request, 'accounts/checkplus_success.html', context)
    except Exception as e:
        print(type(e), e.with_traceback(), e.args)
        raise Exception
