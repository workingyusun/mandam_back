from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'apps.accounts'
    verbose_name = "사용자 관리"

    def ready(self):
        import apps.accounts.signals
