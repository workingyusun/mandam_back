import datetime
from dateutil.relativedelta import relativedelta 

from django.conf import settings

from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.db.models import Avg, Count, Q
from django.db.models import OuterRef, Subquery
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.shortcuts import get_object_or_404

from allauth.account.models import EmailAddress

from rest_auth.views import LoginView
from rest_auth.registration.views import RegisterView

from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from rest_auth.registration.views import SocialLoginView
from rest_auth.registration.serializers import SocialLoginSerializer
from allauth.socialaccount.providers.kakao.views import KakaoOAuth2Adapter
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter

from allauth.socialaccount.providers.oauth2.client import OAuth2Client

from apps.core.utils import make_error_message, get_now, get_utc
from apps.event.models import HostFeedback, Event, LikedEvent, GuestFeedback
from apps.event.serializers import EventListSerializer

from .serializers import ProfileSerializer, MANDAMRegisterSerializer, ProfileWithPhoneNumberSerializer
from .serializers import PhoneNumberSerializer, EmailAddressSerializer, InterestSerializer, HostApplicationSerializer
from .serializers import FeedbackReadOnlySerializer, PeopleReadOnlySerializer, SocialSignUpSerializer
from .serializers import CheckPlusLogSerializer
from .models import Profile, PhoneNumber, Interest, HostApplication, CheckPlusLog


User = get_user_model()

class CheckPlusLogViewSet(viewsets.GenericViewSet, mixins.RetrieveModelMixin):
    queryset = CheckPlusLog.objects.all()
    serializer_class = CheckPlusLogSerializer

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        filter_kwargs = {"reqseq": self.kwargs["pk"]}
        obj = get_object_or_404(queryset, **filter_kwargs)
        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj


class CustomSocialLoginView(SocialLoginView):
    def post(self, request, *args, **kwargs):
        self.request = request
        self.serializer = self.get_serializer(data=self.request.data,
                                              context={'request': request})
        self.serializer.is_valid(raise_exception=True)
        self.login()

        user = self.user

        profile = Profile.objects.filter(user=user).first()

        if not profile:
            serializer = SocialSignUpSerializer(data=self.request.data, context={'request': request})

            try:
                serializer.is_valid(raise_exception=True)
            except Exception as e:
                print(e)
                self.user.delete()
                return Response(make_error_message(300001), status=status.HTTP_400_BAD_REQUEST)

            data = serializer.validated_data
            reqseq = data.get('reqseq')
            cpl = CheckPlusLog.objects.get(reqseq=reqseq)

            if PhoneNumber.objects.filter(phone_number=cpl.phone_number).exists():
                self.user.delete()
                return Response(make_error_message(300002), status=status.HTTP_400_BAD_REQUEST)

            birthdate = cpl.birthdate
            birth_datetime = get_utc(datetime.datetime.strptime(birthdate, "%Y%m%d"))
            now = get_now()
            fourteen = now - relativedelta(years=14)

            if fourteen < birth_datetime:
                user.delete()
                return Response(make_error_message(300003), status=status.HTTP_400_BAD_REQUEST)

            profile = Profile(
                user=user, nickname=data.get('nickname'),
                name=cpl.name,
                birth_year=int(cpl.birthdate[:4]),
                gender=data.get('gender')
            )
            user.email = data.get('email')
            email_address = EmailAddress.objects.filter(user=user).first()
            if email_address:
                email_address.email = data.get('email')
            else:
                email_address = EmailAddress(user=user, email=data.get('email'))
            pn = PhoneNumber(user=user, phone_number=cpl.phone_number)

            profile.save()
            user.save()
            email_address.save()
            pn.save()

        return self.get_response()


class FacebookLogin(CustomSocialLoginView):
    adapter_class = FacebookOAuth2Adapter

    def get_response(self):
        s = ProfileSerializer(instance=self.user.profile)
        data = s.data
        data['key'] = self.token.key
        return Response(data)


class KakaoLogin(CustomSocialLoginView):
    serializer_class = SocialLoginSerializer
    adapter_class = KakaoOAuth2Adapter
    client_class = OAuth2Client

    def get_response(self):
        s = ProfileSerializer(instance=self.user.profile)
        data = s.data
        data['key'] = self.token.key
        return Response(data)


class MANDAMLoginView(LoginView):
    def get_response(self):
        s = ProfileSerializer(instance=self.user.profile)
        data = s.data
        data['key'] = self.token.key
        return Response(data)


class MANADAMRegisterView(RegisterView):
    serializer_class = MANDAMRegisterSerializer


class HostApplicationViewSet(viewsets.GenericViewSet,
                             mixins.UpdateModelMixin,
                             mixins.CreateModelMixin):
    queryset = HostApplication.objects.all()
    serializer_class = HostApplicationSerializer
    permission_classes = [IsAuthenticated, ]

    def retrieve(self, request, pk=None):

        if pk != "me":
            return Response(status=status.HTTP_403_FORBIDDEN)

        user = request.user
        instance = self.get_queryset().filter(user=user).first()

        if not instance:
            return Response(status=status.HTTP_404_NOT_FOUND)

        serializer = self.get_serializer(instance)

        return Response(serializer.data)

    def update(self, request, pk=None, *args, **kwargs):

        if pk != "me":
            return Response(status=status.HTTP_403_FORBIDDEN)

        user = request.user
        instance = self.get_queryset().filter(user=user).first()

        if not instance:
            return Response(status=status.HTTP_404_NOT_FOUND)

        partial = kwargs.pop('partial', False)
        serializer = self.get_serializer(instance,
                                         data=request.data,
                                         partial=partial)

        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)



class InterestViewSet(viewsets.GenericViewSet,):
    queryset = Interest.objects.all()
    serializer_class = InterestSerializer
    permission_classes = [IsAuthenticated, ]

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        self.pagination_class.page_size = 50
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class EmailAddressViewSet(viewsets.GenericViewSet,
                          mixins.UpdateModelMixin):
    queryset = EmailAddress.objects.all()
    serializer_class = EmailAddressSerializer
    permission_classes = [IsAuthenticated, ]

    def update(self, request, pk=None, *args, **kwargs):

        if pk != "me":
            return Response(status=status.HTTP_403_FORBIDDEN)

        user = request.user
        partial = kwargs.pop('partial', False)
        instance = EmailAddress.objects.get(user=user)
        serializer = self.get_serializer(instance,
                                         data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)

        self.perform_update(serializer)
        user.email = instance.email
        user.save()

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class PhoneNumberViewSet(viewsets.GenericViewSet,
                         mixins.UpdateModelMixin):
    queryset = PhoneNumber.objects.all()
    serializer_class = PhoneNumberSerializer
    permission_classes = [IsAuthenticated, ]

    def update(self, request, pk=None, *args, **kwargs):

        if pk != "me":
            return Response(status=status.HTTP_403_FORBIDDEN)

        partial = kwargs.pop('partial', False)
        instance = PhoneNumber.objects.get(user=request.user)
        serializer = self.get_serializer(instance,
                                         data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)


class ProfileViewSet(viewsets.GenericViewSet,
                     mixins.RetrieveModelMixin,
                     mixins.UpdateModelMixin,):

    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = []

    def retrieve(self, request, pk=None):
        if pk == "me":
            if not request.user.is_authenticated:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            instance = request.user.profile
            serializer = ProfileWithPhoneNumberSerializer(instance)
        else:
            instance = self.get_queryset().get(pk=pk)
            serializer = self.get_serializer(instance)

        return Response(serializer.data)

    def update(self, request, pk=None, *args, **kwargs):
        if not request.user.is_authenticated:
            return Response(status=status.HTTP_401_UNAUTHORIZED)

        if pk != "me":
            return Response(status=status.HTTP_403_FORBIDDEN)

        partial = kwargs.pop('partial', False)
        instance = Profile.objects.get(user=request.user)
        serializer = self.get_serializer(instance,
                                         data=request.data,
                                         partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        if getattr(instance, '_prefetched_objects_cache', None):
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)

    @action(methods=["get"], detail=True) #permission_classes=[IsAuthenticated])
    def host_rate(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if pk == "me":
            if not request.user.is_authenticated:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            profile = request.user.profile
        else:
            profile = Profile.objects.filter(pk=pk).first()

        if not profile:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if not profile.is_host:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # if request.user == profile.user:
        #     return Response(status=status.HTTP_403_FORBIDDEN)

        data = HostFeedback.objects.filter(host=profile.user).aggregate(
            overall=Avg('rate'),
            count_of_satisfaction_e=Count('pk', filter=Q(rate_of_satisfaction='E')),
            count_of_satisfaction_g=Count('pk', filter=Q(rate_of_satisfaction='G')),
            count_of_satisfaction_b=Count('pk', filter=Q(rate_of_satisfaction='B')),
            count_of_program_e=Count('pk', filter=Q(rate_of_program='E')),
            count_of_program_g=Count('pk', filter=Q(rate_of_program='G')),
            count_of_program_b=Count('pk', filter=Q(rate_of_program='B')),
            count_of_space_e=Count('pk', filter=Q(rate_of_space='E')),
            count_of_space_g=Count('pk', filter=Q(rate_of_space='G')),
            count_of_space_b=Count('pk', filter=Q(rate_of_space='B')),
        )
        return Response(data, status=status.HTTP_200_OK)


    @action(
        methods=["get"], detail=True, #permission_classes=[IsAuthenticated],
        serializer_class=FeedbackReadOnlySerializer)
    def host_reviews(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if pk == "me":
            if not request.user.is_authenticated:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            profile = request.user.profile
        else:
            profile = Profile.objects.filter(pk=pk).first()


        if not profile:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if not profile.is_host:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # if request.user == profile.user:
        #     return Response(status=status.HTTP_403_FORBIDDEN)

        queryset = HostFeedback.objects.filter(
            host=profile.user).select_related('user__profile').order_by('-created_at')

        self.pagination_class.page_size = 5

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # @method_decorator(cache_page(60))
    @action(methods=["get"], detail=True)
    def host_events(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if pk == "me":
            if not request.user.is_authenticated:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            profile = request.user.profile
        else:
            profile = Profile.objects.filter(pk=pk).first()

        if not profile:
            return Response(status=status.HTTP_404_NOT_FOUND)

        if not profile.is_host:
            return Response(status=status.HTTP_404_NOT_FOUND)

        queryset = Event.objects.filter(user=profile.user, is_verified=True).prefetch_related("liked_events")

        if request.user.is_authenticated:
            user_pk = request.user.pk
        else:
            # make this null
            user_pk = 0

        liked_event = LikedEvent.objects.filter(event=OuterRef('id'), user__pk=user_pk)
        queryset = queryset.annotate(liked_event=Subquery(liked_event.values('id')[:1]))
        queryset = queryset.order_by('-created_at')

        self.pagination_class.page_size = 4
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = EventListSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = EventListSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


    @action(methods=["get"], detail=True) #permission_classes=[IsAuthenticated])
    def guest_rate(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if pk == "me":
            if not request.user.is_authenticated:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            profile = request.user.profile
        else:
            #return Response(status=status.HTTP_401_UNAUTHORIZED)
            profile = Profile.objects.filter(pk=pk).first()

        if not profile:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # if request.user == profile.user:
        #     return Response(status=status.HTTP_403_FORBIDDEN)

        data = GuestFeedback.objects.filter(guest=profile.user).aggregate(
            overall=Avg('rate')
        )

        return Response(data, status=status.HTTP_200_OK)


    @action(methods=["get"], detail=True,
            serializer_class=FeedbackReadOnlySerializer) #permission_classes=[IsAuthenticated])
    def guest_reviews(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if pk == "me":
            if not request.user.is_authenticated:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            profile = request.user.profile
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)
            # profile = Profile.objects.filter(pk=pk).first()

        if not profile:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # if request.user == profile.user:
        #     return Response(status=status.HTTP_403_FORBIDDEN)

        queryset = GuestFeedback.objects.filter(
            guest=profile.user).select_related('user__profile').order_by('-created_at')

        self.pagination_class.page_size = 2

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # @method_decorator(cache_page(60))
    @action(methods=["get"], detail=True)
    def guest_events(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if pk == "me":
            if not request.user.is_authenticated:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            profile = request.user.profile
        else:
            profile = Profile.objects.filter(pk=pk).first()

        if not profile:
            return Response(status=status.HTTP_404_NOT_FOUND)

        queryset = Event.objects.filter(
            schedules__registrations__user=profile.user, schedules__registrations__status="payment_success",
            is_verified=True).prefetch_related("liked_events")

        if request.user.is_authenticated:
            user_pk = request.user.pk
        else:
            # make this null
            user_pk = 0

        liked_event = LikedEvent.objects.filter(event=OuterRef('id'), user__pk=user_pk)
        queryset = queryset.annotate(liked_event=Subquery(liked_event.values('id')[:1]))
        queryset = queryset.order_by('-created_at')

        self.pagination_class.page_size = 4
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = EventListSerializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = EventListSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    # @method_decorator(cache_page(60))
    @action(methods=["get"], detail=True, serializer_class=PeopleReadOnlySerializer)
    def people(self, request, *args, **kwargs):
        pk = kwargs.get("pk", None)

        if pk == "me":
            if not request.user.is_authenticated:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            profile = request.user.profile
        else:
            profile = Profile.objects.filter(pk=pk).first()

        if not profile:
            return Response(status=status.HTTP_404_NOT_FOUND)

        # as a guest
        host_profiles = Profile.objects.filter(
            user__events__schedules__registrations__user=profile.user,
            user__events__schedules__registrations__status="payment_success")
        # as a host
        guest_profiles = Profile.objects.filter(
            user__registrations__schedule__event__user=profile.user,
            user__registrations__status="payment_success")

        queryset = host_profiles.union(guest_profiles).order_by('-created_at')
        self.pagination_class.page_size = 20

        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)
