import datetime

def year_choices():
    return [(r, r) for r in range(1930, datetime.date.today().year+1)]

def current_year():
    # 현재 20살 나이
    return datetime.date.today().year - 20
