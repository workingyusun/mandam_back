import datetime
from dateutil.relativedelta import relativedelta 

from django.db import transaction

from allauth.account.adapter import get_adapter
from allauth.account.models import EmailAddress
from allauth.account.utils import setup_user_email

from rest_auth.registration.serializers import SocialLoginSerializer
from rest_framework import serializers, permissions
from rest_framework.validators import UniqueValidator

from rest_auth.registration.serializers import RegisterSerializer

from apps.core.utils import get_now, get_kt, get_utc
from .models import Profile, PhoneNumber, Interest, HostApplication, CheckPlusLog


class HostApplicationSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    profile_image = serializers.ImageField(write_only=True, required=False)
    profile_self_text = serializers.CharField(write_only=True, required=True)
    profile_nickname = serializers.CharField(write_only=True, required=True)

    class Meta:
        model = HostApplication
        fields = "__all__"
        read_only_fields = ["is_verified"]

    def create(self, validated_data):
        user = self.context['request'].user
        instance = HostApplication.objects.filter(user=user).first()
        if instance:
            raise serializers.ValidationError("이미 호스트 신청을 하셨습니다.")
        profile = user.profile
        profile_image = validated_data.pop("profile_image", None)
        if profile_image:
            profile.image = profile_image
        profile.self_text = validated_data.pop("profile_self_text")
        profile.nickname = validated_data.pop('profile_nickname')
        profile.save()
        return super(HostApplicationSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        validated_data.pop('profile', None)  # prevent myfield from being updated
        return super(HostApplicationSerializer, self).update(instance, validated_data)


class EmailAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailAddress
        fields = ["email", ]


class InterestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interest
        fields = "__all__"


class ProfileReadOnlySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    name = serializers.CharField(read_only=True)
    nickname = serializers.CharField(read_only=True)
    image = serializers.ImageField(read_only=True)


class ProfileHostReadOnlySerializer(ProfileReadOnlySerializer):
    gender = serializers.CharField(read_only=True)
    birth_year = serializers.IntegerField(read_only=True)
    email = serializers.EmailField(read_only=True, source="user.email")
    phone_number = serializers.CharField(read_only=True, source='user.phonenumber.phone_number')


class PhoneNumberSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = PhoneNumber
        fields = "__all__"

    def validate_phone_number(self, value):
        return value


class ProfileSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    phone_number = serializers.SerializerMethodField()
    interest_ids = serializers.PrimaryKeyRelatedField(
        many=True, write_only=True,
        required=False, queryset=Interest.objects.all(), source="interests")
    interests = InterestSerializer(many=True, read_only=True)

    def get_phone_number(self, instance):
        user = instance.user
        phone_number = user.phonenumber
        return PhoneNumberSerializer(phone_number, read_only=True).data

    class Meta:
        model = Profile
        fields = "__all__"
        read_only_fields = ['is_host', ]


class ProfileWithPhoneNumberSerializer(ProfileSerializer):
    phone_number = serializers.SerializerMethodField()
    has_usable_password = serializers.SerializerMethodField()

    def get_phone_number(self, instance):
        user = instance.user
        phone_number = user.phonenumber
        return PhoneNumberSerializer(phone_number, read_only=True).data

    def get_has_usable_password(self, instance):
        return instance.user.has_usable_password()


class MANDAMRegisterSerializer(RegisterSerializer):
    profile = ProfileSerializer()
    reqseq = serializers.CharField(write_only=True)
    #phone_number = PhoneNumberSerializer()

    def get_cleaned_data(self):
        return {
            'username': self.validated_data.get('username', ''),
            'password1': self.validated_data.get('password1', ''),
            # 'nickname': self.validated_data.get('nickname', ''),
            'reqseq': self.validated_data.get('reqseq', ''),
            'email': self.validated_data.get('email', ''),
            'profile': self.validated_data.get('profile', ''),
            # 'phone_number': self.validated_data.get('phone_number', '')
        }


    def validate_reqseq(self, value):
        if not value:
            raise serializers.ValidationError('본인인증이 필요합니다.')
        cpl = CheckPlusLog.objects.filter(reqseq=value).first()
        if not cpl:
            raise serializers.ValidationError("본인 인증이 필요합니다.")
        phone_number = cpl.phone_number
        if PhoneNumber.objects.filter(phone_number=phone_number).exists():
            raise serializers.ValidationError("이 번호는 이미 존재합니다.")
        birthdate = cpl.birthdate
        birth_datetime = get_utc(datetime.datetime.strptime(birthdate, "%Y%m%d"))
        now = get_now()
        fourteen = now - relativedelta(years=14)
        if fourteen < birth_datetime:
            raise serializers.ValidationError("만 14세 이상만 가입 가능합니다.")
        return value

    def custom_signup(self, request, user):
        profile = self.cleaned_data.get('profile')
        # phone_number = self.cleaned_data.get('phone_number')
        reqseq = self.cleaned_data.get('reqseq')

        request.user = user
        context = {'request': request}

        cpl = CheckPlusLog.objects.filter(reqseq=reqseq).first()
        profile['name'] = cpl.name
        profile['birth_year'] = int(cpl.birthdate[:4])
        profile_serializer = ProfileSerializer(data=profile, context=context)
        phone_number = {"user": user, "phone_number": cpl.phone_number}
        phone_number_serializer = PhoneNumberSerializer(data=phone_number, context=context)
        profile_serializer.is_valid(raise_exception=True)
        phone_number_serializer.is_valid(raise_exception=True)

        with transaction.atomic():
            profile_serializer.save()
            phone_number_serializer.save()

    def save(self, request):
        adapter = get_adapter()
        user = adapter.new_user(request)
        self.cleaned_data = self.get_cleaned_data()
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        setup_user_email(request, user, [])
        return user


class FeedbackReadOnlySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    profile_id = serializers.IntegerField(read_only=True, source='user.profile.id')
    profile_image = serializers.ImageField(read_only=True, source='user.profile.image')
    profile_name = serializers.CharField(read_only=True, source='user.profile.name')
    rate = serializers.IntegerField(read_only=True)
    feedback = serializers.CharField(read_only=True)


class PeopleReadOnlySerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    image = serializers.ImageField(read_only=True)


class SocialSignUpSerializer(serializers.Serializer):
    nickname = serializers.CharField()
    email = serializers.EmailField()
    reqseq = serializers.CharField()
    # phone_number = serializers.CharField()
    # birth_year = serializers.IntegerField()
    gender = serializers.CharField()

    def validate_email(self, value):
        user = self.context['request'].user
        if EmailAddress.objects.exclude(user=user).filter(email=value).exists():
            raise serializers.ValidationError('이 이메일이 이미 존재합니다.')
        return value

    def validate_reqseq(self, value):
        if not CheckPlusLog.objects.filter(reqseq=value).exists():
            raise serializers.ValidationError("본인인증을 수행해주세요")
        return value


class CheckPlusLogSerializer(serializers.ModelSerializer):
    class Meta:
        model = CheckPlusLog
        fields = ["name", "phone_number", "reqseq", "result"]
