import datetime
import hashlib
import requests
import json
import base64
from random import randint

from Crypto.Cipher import AES


#from .models import BillKeyLog, BillKey


BS = AES.block_size
pad = lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS)
#unpad = lambda s: s[0:-ord(s[-1])]

HOST = "https://webapi.nicepay.co.kr/webapi/billing"
MID = "nictest04m"
MOID = "1004"
MERCHANT_KEY = "b+zhZ4yOZ7FsH8pm5lhDfHZEb79tIwnjsdA0FBXh86yLc6BJeFVrZFXhAoJ3gEWgrWwN+lJMV0W4hvDdbe4Sjw=="

bill_key = ""

class AESCipher:
    def __init__(self, key):
        self.key = key

    def encrypt(self, raw):
        """
        Returns hex encoded encrypted value!
        """
        raw = pad(raw)
        cipher = AES.new(self.key, AES.MODE_ECB)
        return cipher.encrypt(raw).hex()

    # def decrypt(self, enc):
    #     """
    #     Requires hex encoded param to decrypt
    #     """
    #     enc = enc.decode("hex")
    #     cipher = AES.new(self.key, AES.MODE_ECB)
    #     return unpad(cipher.decrypt(enc))


def get_edi_date():
    return datetime.datetime.today().strftime("%Y%m%d%H%M%S")


def get_sign_data(string):

    encoded_str = string.encode()
    encrypted_data = hashlib.sha256(encoded_str).hexdigest()
    return encrypted_data

def get_tid():
    # 01: 지불수단 (신용카드) -> 체크카드도 가능 함
    # 16: 빌링
    return MID + "01" + "16" + datetime.datetime.today().strftime("%y%m%d%H%M%S") + str(randint(1000,9999))

def nice_request(url, data={}, params={}, raise_error=False):
    headers = {
        'Content-type': 'application/x-www-form-urlencoded',
        'charset': 'euc-kr'
    }
    response = requests.post(
        url=url,
        data=data,
        params=params,
        headers=headers
    )
    if raise_error:
        response.raise_for_status()
    if response.ok:
        return response.json()
    return

def payment(bill_key):
    edi_date = get_edi_date()
    string = MID + edi_date + MOID + str(1000) + bill_key + MERCHANT_KEY
    sign_data = get_sign_data(string)

    params = {
        "BID": bill_key,
        "MID": MID,
        "TID": get_tid(),
        "EdiDate": edi_date,
        "Moid": MOID,
        "Amt": 1000,
        "GoodsName": "test",
        "SignData": sign_data,
        "CardInterest": "0",
        "CardQuota": "00",
    }
    url = "https://webapi.nicepay.co.kr/webapi/billing/billing_approve.jsp"
    res = nice_request(url=url, params=params)
    return res


def cancel(tid, cancel_amt, partial_cancel_code=0):
    edi_date = get_edi_date()
    string = MID + str(cancel_amt) + edi_date + MERCHANT_KEY
    sign_data = get_sign_data(string)

    params = {
        "TID": tid,
        "MID": MID,
        "Moid": MOID,
        "CancelAmt": cancel_amt,
        "CancelMsg": "고객 요청",
        "PartialCancelCode": partial_cancel_code,
        "EdiDate": edi_date,
        "SignData": sign_data
    }
    url = "https://webapi.nicepay.co.kr/webapi/cancel_process.jsp"
    res = nice_request(url=url, params=params)
    return res


if __name__ == "__main__":
    res = payment(bill_key)
    print(cancel(res.get("TID"), 1000))