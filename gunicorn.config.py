import multiprocessing
import os

GAE_ENV = os.environ.get('GAE_ENV')

workers = multiprocessing.cpu_count() * 2 + 1
if GAE_ENV and GAE_ENV == 'standard':
    workers = 1

print(f'* NUM OF WORKERS: {workers}')
